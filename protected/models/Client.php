<?php

class Client extends CActiveRecord {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function tableName() {
        return '{{clients}}';
    }
    
    public function rules(){
        return array(
            array('name', 'unique', 'caseSensitive'=>true),
            array('name', 'required'),
            array('token', 'unique', 'caseSensitive'=>true),
            array('token' , 'required')
        );
    }
    
    public static function isAuthorized($token) {
        $record = Client::model()->find(array('condition' => "`token` = '$token'"));
        return $record !== null;
    }
    
    public function regenerateToken() {
        $this->token = uniqid("odysseus-", true);
    }
    
}

?>
