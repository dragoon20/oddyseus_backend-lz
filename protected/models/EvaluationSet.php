<?php

class EvaluationSet extends CActiveRecord {
    
    private $_config;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{evaluation_sets}}';
    }
    
    public function getConfig($key) {
        if (isset($this->_config[$key]))
            return $this->_config[$key];
        else
            return null;
    }
    
    public function rules() {
        return array(
            array('grader_type_id', 'required'),
            array('compiler_type_id', 'required'),
            array('languages', 'required'),
            array('file', 'safe'),
            array('client_id', 'safe'),
            array('date_created', 'safe'),
        );
    }
    
    public function setConfig($key , $value) {
        $this->_config[$key] = $value;
    }
    
    public function setConfigs($configs) {
        foreach ($configs as $key => $config) {
            $this->_config[$key] = $config;
        }
    }
    
    protected function afterFind() {
        $this->_config = json_decode($this->config, true);
        return parent::afterFind();
    }
    
    protected function beforeSave() {
        $this->config = json_encode($this->_config);
        return parent::beforeSave();
    }

}

?>
