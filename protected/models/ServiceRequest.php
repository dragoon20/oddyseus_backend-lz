<?php

class ServiceRequest extends CActiveRecord {

    const STATUS_RECEIVED = 0;
    const STATUS_DISPATCHED = 1;
    const STATUS_PROCESSED = 2;
    const STATUS_REPLIED = 3;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Membuat record ServiceRequest dari HttpRequest
     * @param type $request 
     */
    public static function createFromRequest($pRequest , $parameter) {
        $request = new ServiceRequest();
        $request->host_ip_address = $pRequest->getUserHostAddress();
        $request->user_agent = $pRequest->getUserAgent();
        $request->path_info = $pRequest->getPathInfo();
        $request->parameter = json_encode($parameter);
        $request->query_string = $pRequest->getQueryString();
        $request->status = ServiceRequest::STATUS_RECEIVED;
        $request->callback_url = $parameter['callbackurl'];
        return $request;
    }

    public function tableName() {
        return '{{service_requests}}';
    }

}

?>
