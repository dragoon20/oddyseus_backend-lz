<?php

abstract class CompilerBase {

    protected $targetFilePath;
    protected $outputFilePath;
    protected $requestModel;
    protected $compileOutput;
    protected $compileReturnValue;
    protected $executionScriptPath;
    protected $gradingDirectory;

    public function __construct($request) {
        $this->targetFilePath = "";
        $this->requestModel = $request;
        //$this->requestModel->dispatch_time = new CDbExpression("NOW()");
        $this->requestModel->dispatch_time = date("Y-m-d H:i:s");
        $this->requestModel->save();
    }

    protected abstract function beforeCompile();

    protected abstract function afterCompile();

    protected abstract function doCompile();
    
    protected abstract function generateExecutionScript();
    
    public abstract function setTarget($target);
    
    public abstract function addCapabilities($cap);
    
    private function recursiveChmod ($path, $filePerm=0644, $dirPerm=0755) 
    {
        // Check if the path exists
        if (!file_exists($path)) {
            return(false);
        }
 
        // See whether this is a file
        if (is_file($path)) {
            // Chmod the file with our given filepermissions
        	chmod($path, $filePerm);
        // If this is a directory...
        } elseif (is_dir($path)) {
            // Then get an array of the contents
            $foldersAndFiles = scandir($path);
 
            // Remove "." and ".." from the list
            $entries = array_slice($foldersAndFiles, 2);
 
            // Parse every result...
            foreach ($entries as $entry) {
                // And call this function again recursively, with the same permissions
                $this->recursiveChmod($path."/".$entry, $filePerm, $dirPerm);
            }
 
            // When we are done with the contents of the directory, we chmod the directory itself
            chmod($path, $dirPerm);
        }
 
        // Everything seemed to work out well, return true
        return(true);
    }
    
    private function prepareGradingDirectory() {
        $this->gradingDirectory = Yii::app()->params['config']['compiler']['temp_dir'] . "/" . $this->requestModel->id;
        
        if (!file_exists($this->gradingDirectory))
            mkdir($this->gradingDirectory);

        //extract evaluation set data
    	$gradingDir = $this->getGradingDirectory();
        $tmpTestCasePath = $gradingDir . "/tc.zip";
        file_put_contents($tmpTestCasePath, $this->getRequestModel()->evaluationset->file);
        exec("unzip -o " . $tmpTestCasePath . " -d $gradingDir");
        $this->recursiveChmod($gradingDir."/tc", 777);
    }

    public final function compile() {
        $this->prepareGradingDirectory();
        
        $this->beforeCompile();

        ///Init
        $this->compileOutput = "";
        $this->compileReturnValue = -1;
        $this->outputFilePath = "";

        $this->doCompile();
        $this->generateExecutionScript();
        $this->afterCompile();
    }
    
    public final function getGradingDirectory() {
        return $this->gradingDirectory;
    }

    public final function getOutputFilePath() {
        return $this->outputFilePath;
    }

    public final function getCompileOutput() {
        return $this->compileOutput;
    }

    public final function getCompileReturnValue() {
        return $this->compileReturnValue;
    }
    
    public final function getExecutionScriptPath() {
        return $this->executionScriptPath;
    }
    
    public final function getRequestModel() {
        return $this->requestModel;
    }
    
    public function cleanup() {
        /*if (file_exists($this->outputFilePath))
            unlink ($this->outputFilePath);*/
        if (file_exists($this->gradingDirectory))
           exec("rm -rf " . $this->gradingDirectory);
    }

}

?>
