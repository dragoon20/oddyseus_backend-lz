<?php $this->setPageTitle("TOKI Learning Center"); ?>
<?php
Yii::app()->clientScript->registerMetaTag(Yii::app()->name, NULL, NULL, array('property' => 'og:title'));
Yii::app()->clientScript->registerMetaTag(Yii::app()->name, NULL, NULL, array('property' => 'og:site_name'));
Yii::app()->clientScript->registerMetaTag('website', NULL, NULL, array('property' => 'og:type'));
Yii::app()->clientScript->registerMetaTag(Yii::app()->params->adminEmail, NULL, NULL, array('property' => 'og:email'));
Yii::app()->clientScript->registerMetaTag(Yii::app()->params->facebook['app_id'], NULL, NULL, array('property' => 'fb:app_id'));
Yii::app()->clientScript->registerMetaTag(Yii::app()->request->hostInfo . Yii::app()->request->baseUrl . '/images/logo-toki-60.png', NULL, NULL, array('property' => 'og:image'));
?>
<?php
Yii::app()->clientScript->registerCss('guest_home_css', '
    #guest-wrapper {}
    #information-wrapper {width:620px;float:left;}
    #forms-wrapper {width:320px;float:right;padding:0px;}
    #signin-wrapper {border: 1px solid #bbb;margin:0px 5px 5px 5px;padding:3px;}
    #facebook-wrapper {margin:3px 5px 5px 5px;background:#fff;}
    #twitter-wrapper {margin:3px 5px 5px 5px;background:#fff;}
    #register-wrapper {border: 1px solid #bbb;margin:5px;padding:3px;}
    #spacer {clear:both;}
    #welcome, #announcements {border:1px solid #aaa;padding:2px 10px;margin-bottom:15px;}
    #announcements {margin-bottom:0px}
    #welcome h2, #announcements h2 {border-bottom:1px solid #000;}
    #socmed-wrapper {clear:both;padding:0px;margin:0px 5px 7px 5px;}
    #socmed {list-style:none;margin:0px;padding:0px;}
    #socmed li {float:left;}
');
?>
<div id="guest-wrapper">
    <div id="information-wrapper">
    </div>

    <div id="forms-wrapper">
        
        <div id="signin-wrapper">
            <?php $this->renderPartial('_loginform', array('loginform' => $loginform)); ?>
        </div>
        <div id="register-wrapper">
            <?php $this->renderPartial('_registerform', array('user' => $user, 'regHasError' => $regHasError)); ?>
        </div>
        
    </div>
    <div id="spacer"></div>
</div>
