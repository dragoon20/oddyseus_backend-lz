<?php $this->setPageTitle("Status Dispatcher");?>
<div class="dtable">
    <div class="drow">
        <span>
            <strong>Status</strong>
        </span>
        <span>
            <?php if ($this->getPid()) : ?>
            Running (PID : <?php echo $this->getPid();?>) <a href="<?php echo $this->createUrl("kill")?>">Kill</a>
            <?php else :?>
            Stopped <a href="<?php echo $this->createUrl("start")?>">Start</a>
            <?php endif;?>
        </span>
    </div>
</div>
<h3>Konfigurasi</h3>
<?php echo CHtml::beginForm();?>
<div class="dtable">
    <?php foreach ($prop->propertyNames() as $key) : ?>
    <div class="drow">
        <span>
            <strong><?php echo $key;?></strong>
        </span>
        <span>
            <input type="text" name="Prop[<?php echo $key;?>]" value="<?php echo $prop->getProperty($key);?>" />
        </span>
    </div>
    <?php endforeach;?>
</div>
<?php echo CHtml::submitButton("Submit");?>
<?php echo CHtml::endForm();?>