<?php $this->setPageTitle("Perihal"); ?>
<?php Yii::app()->clientScript->registerCss('about-style' , "
	span.description {width: 60%; height: 100%; vertical-align: top;}
	span.right {text-align: right;}
	img.photo {width: 130px;}
");?>
<p style="text-align:justify">
    Situs <strong>TOKI Learning Center</strong> dikembangkan dan dibina oleh <a href="http://toki.if.itb.ac.id">TOKI Biro Institut Teknologi Bandung</a>.
    Situs ini dikembangkan dengan tujuan untuk memperkenalkan dan membina siswa-siswi Indonesia di dalam ilmu informatika.
    Soal-soal yang ada di situs ini merupakan kumpulan dari soal-soal yang pernah digunakan dalam kegiatan yang dilaksanakan
    oleh <a href="http://www.tokinet.org">Tim Olimpade Komputer Indonesia</a>. Soal-soal tersebut merupakan hak milik penciptanya.
</p>
<div style="width:400px; margin: auto;" id="__ss_10166450"> <strong style="display:block;margin:12px 0 4px"><a href="http://www.slideshare.net/petrabarus/setahun-toki-learning-center" title="Setahun TOKI Learning Center" target="_blank">Setahun TOKI Learning Center</a></strong> <iframe src="http://www.slideshare.net/slideshow/embed_code/10166450" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe> <div style="padding:5px 0 12px"> View more <a href="http://www.slideshare.net/" target="_blank">presentations</a> from <a href="http://www.slideshare.net/petrabarus" target="_blank">Petra Barus</a> </div> </div>
<h2>Pengembang</h2>
<div class="dtable">
	<div class="drow">
		<span>
			<img src="images/about/petra.jpg" class="photo" alt="" />
		</span>
		<span class="description left">
			<h3>Petra Novandi Barus, S.T, M.T</h3>
			<em>Lead Developer 2008-sekarang</em><br /><br />
			<p>
				Petra barus saja menyelesaikan studinya di Program Studi Magister Informatika ITB, di mana dia juga menyelesaikan studi Sarjananya. Dalam menyelesaikan studi Sarjananya, Petra merintis pengembangan TOKI Learning Center sebagai proyek tugas akhirnya. Sekarang ia menjabat sebagai <i>Director of Engineering</i> di <a href="http://urbanindo.com">UrbanIndo.com</a>. Petra memiliki ketertarikan pada pengembangan aplikasi web dan komputasi paralel.
			</p>
			<p>
				<strong>petra.barus@gmail.com</strong>
			</p>
		</span>
	</div>
	<div class="drow">
		<span>

		</span>
		<span class="description right">
			<h3>Karol Danutama</h3>
			<em>Developer 2009-sekarang</em><br /><br />
			<p>
				Karol adalah mahasiswa tingkat akhir di Program Studi Teknik Informatika ITB. Sebagai peraih medali perunggu IOI 2007, Karol terpanggil untuk terlibat dalam pengembangan TOKI Learning Center saat Petra merintis TOKI Learning Center versi awal. Sampai saat ini, ia terus aktif mengembangkan TOKI Learning Center. Karol memiliki ketertarikan dalam pengembangan aplikasi web dan sistem terdistribusi.
			</p>
			<p>
				<strong>karoldanutama@gmail.com</strong>
			</p>
		</span>
		<span>
			<img src="images/about/karol.jpg" class="photo" alt="" />
		</span>
	</div>
	<div class="drow">
		<span>
			<img src="images/about/adin.jpg" class="photo" alt="" />
		</span>
		<span class="description left">
			<h3>Muhammad Adinata</h3>
			<em>Associate Developer 2010-sekarang</em><br /><br />
			<p>
				Adinata, atau kerap dipanggil Adin, adalah alumni Tim Olimpiade Komputer Indonesia tahun 2009. Saat ini Adin sedang menempuh studi di Program Studi Teknik Informatika ITB.
			</p>
			<p>
				<strong>mail.dieend@gmail.com</strong>
			</p>
		</span>
		<span>
			
		</span>
	</div>
</div>
