<?php $this->setPageTitle("Open OSN");?>
<?php Yii::app()->clientScript->registerCss('open-osn-css', '
  #openosn h1 {border-bottom:1px solid #555;font-size:27px;}
');?>
<div id="openosn">
<h1>OSN Informatika Terbuka 2010</h1>

Dengan ini, diumumkan bahwa pada tahun ini akan diadakan OSN Informatika Terbuka 2010 untuk memberikan kesempatan kepada peserta yang karena satu dan lain hal tidak diundang ke OSN 2010 yang diadakan di Medan, Sumatera Utara.
<br/>
Perhatikan berkas <a href="http://tokinet.org/tokifiles/Prosedur_OSN_Informatika_Terbuka_2010_v-3_Peserta.pdf">prosedur peserta</a> (prosedur tersebut bersifat fleksibel, yang berarti bahwa mungkin ada perubahan yang diinformasikan kemudian).
<br/>
Hal yang sangat penting dari berkas prosedur tersebut antara lain adalah:
<ol>
 <li>Soal, pembobotan nilai, penentuan pemenang, dan jadwal OSN Terbuka menyerupai OSN 2010 di Medan.</li>
 <li>Terdapat 2 macam peserta, online atau onsite. Onsite hanya dapat diikuti siswa SMP/sederajat dan SMA/sederajat.</li>
 <li>Terdapat 6 pilihan lokasi keikutsertaan untuk peserta onsite: UI, ITB, UGM, ITS, USU, UNAND.</li>

 <li>Registrasi dilakukan melalui email open.osn@gmail.com (bukan menghubungi langsung lokasi pelaksanaan ataupun kontak lainnya) selambat-lambatnya 1 Agustus 2010 (first come first serve) dengan menyertakan informasi Nama Lengkap, Tempat Tanggal Lahir, Alamat Rumah, Asal Sekolah, Alamat Sekolah, dan Pilihan Lokasi Pelaksanaan.</li>
</ol>
Demikian pengumuman ini. Selamat berkompetisi, jadilah yang terbaik.
<br/>
Pertanyaan seputar OSN Terbuka ini dapat juga dikirimkan melalui email ke open.osn@gmail.com.<br>
<br><i>-: Brian Marshal</i><br>

</div><br/>
<a href="http://lc.toki.if.itb.ac.id/lx/"><strong>Ke Halaman Utama</strong></a>
