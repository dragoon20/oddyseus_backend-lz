<?php $this->setPageTitle("Kontes - ".$model->name);?>
<div class="dtable">
    <div class="drow">
        <span class="shead">Sifat</span>
        <span><?php echo ($model->span_type == Contest::CONTEST_SPAN_TYPE_FIXED) ? "Waktu tetap" : "Waktu virtual";?></span>    
    </div>
    <div class="drow">
        <span class="shead">Waktu Dibuka</span>
        <span><?php echo $model->start_time;?></span>    
    </div>
    <div class="drow">
        <span class="shead">Waktu Ditutup</span>
        <span><?php echo $model->end_time;?></span>    
    </div>
    <?php if ($model->span_type == Contest::CONTEST_SPAN_TYPE_VIRTUAL) : ?>
    <div class="drow">
        <span class="shead">Durasi</span>
        <span><?php echo $model->getConfig('timespan');?> menit</span>    
    </div>
    <?php endif;?>
</div>
<hr /> 
<?php if ($model->hasEnded() && false) : ?>
<div id="contest-expired" class="error">
    Kontes ditutup pada <?php echo CDateHelper::timespanAbbr($model->end_time);?>
</div>
<?php else :?>
    <?php 
    if ($model->isMember(Yii::app()->user))
        if ($model->hasStarted()) 
            echo CHtml::link('Masuk' , $this->createUrl('contest/contest/signin' , array('contestid' => $model->id)) , array('class' => 'linkbutton' , 'onclick' => 'return confirm("Apakah anda yakin ingin memasuki kontes? Pastikan anda membaca peraturan/pengumuman. Timer akan dimulai begitu anda masuk.");'));
        else
            echo '<div class="error">Kontes dibuka pada '. CDateHelper::timespanAbbr($model->start_time) .'</div>';
    else if ($model->isRegistrant(Yii::app()->user))
        echo "<div class=\"error\">Pendaftaran anda masih menunggu konfirmasi</div>";
    ?>
<?php endif;?>
<?php;?>
<br /><br />
<h3>Pengumuman</h3>
<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_news',   // refers to the partial view named '_post'
    'emptyText' => 'Belum ada pengumuman',
    'summaryText' => 'Menampilkan {end} pengumuman dari {count}. ',
    'enableSorting' => false,
    'cssFile' => Yii::app()->request->baseUrl.'/css/yii/listview/style.css',
));
?>