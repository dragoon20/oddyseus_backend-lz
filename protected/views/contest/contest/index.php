<?php $this->setPageTitle("Kontes");?>
<div>
    <h4>Kontes yang kamu ikuti</h4>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $activeContestDataProvider,
        'itemView' => '_contest',
        'summaryText' => 'Menampilkan {start}-{end} dari {count}.',
        'emptyText' => 'Kamu tidak sedang mengikuti kontes apapun',
        'cssFile' => Yii::app()->request->baseUrl.'/css/yii/listview/style.css',
    ));
    ?>
</div>
<br/>
<?php echo CHtml::link('Lihat semua kontes yang pernah kamu ikut', $this->createUrl('list', array('filter' => 'active')));?>
<hr/>
<div>
    <h4>Kontes yang dapat diikuti</h4>
    <?php if (Yii::app()->user->hasFlash('contestRegisterSucces')):?>
    <?php $contest = Yii::app()->user->getFlash('contestRegisterSucces');?>
        <div class="errorMessage">
            Kamu telah mendaftar di kontes <strong><?php echo $contest->name;?></strong>. Pendaftaran kamu akan segera dikonfirmasi.
        </div>
        <br/>
    <?php endif;?>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $publicContestDataProvider,
        'itemView' => '_opencontest',
        'summaryText' => 'Menampilkan {start}-{end} dari {count}.',
        'emptyText' => 'Tidak kontes terbuka yang sedang berlangsung',
        'cssFile' => Yii::app()->request->baseUrl.'/css/yii/listview/style.css',
    ));
    ?>
</div>
<br/>
<?php echo CHtml::link('Lihat semua kontes terbuka yang pernah berlangsung', $this->createUrl('list', array('filter' => 'all')));?>
