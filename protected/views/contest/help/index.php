<?php $this->setPageTitle("Bantuan");?>
<h2 class="title" style="font-size:18px;">Petunjuk Penggunaan</h2>
<div>
    <p>Penjelasan menu pada panel kontes</p>
    <ol>
        <li><strong>Pengumuman</strong>
            <p>Pada menu ini kamu dapat melihat berita-berita yang diumumkan oleh para pembina.
               Periksalah pengumuman sesering mungkin.</p>
        </li>
        <li><strong>Soal</strong>
            <p>Pada menu ini terdapat daftar soal yang harus kamu kerjakan pada kontes ini. Klik pada salah satu soal untuk mengerjakannya.</p>
        </li>
        <li><strong>Jawaban</strong>
            <p>Pada menu ini kamu dapat melihat daftar jawaban yang telah kamu kumpulkan pada soal-soal yang disediakan.
            Setiap jawaban memiliki beberapa status
            <ul style="display:table;">
                <li style="display:table-row"><strong style="display:table-cell;width:100px;">Graded</strong> Status ini berarti jawaban kamu telah selesai diperiksa.
                </li>
                <li style="display:table-row"><strong style="display:table-cell">Pending</strong> Status ini berarti jawaban kamu sedang menunggu untuk diperiksa setelah jawaban lain selesai diperiksa.
                </li>
                <li style="display:table-row"><strong style="display:table-cell">No Grade</strong> Status ini berarti jawaban kamu tidak langsung diperiksa (misal: diperiksa setelah kontes berakhir)
                </li>
                <li style="display:table-row"><strong style="display:table-cell">Error</strong> Harap lapor pembina jika menemukan status ini.
                </li>
            </ul><br/>
            </p>
        </li>
        <li><strong>Klarifikasi</strong>
            <p>Pada bagian ini kamu dapat meminta klarifikasi kepada pembina jika kamu merasa tidak jelas mengenai soal tertentu.
            Bagian ini juga menampilkan pertanyaan-pertanyaan yang diajukan oleh peserta lain sehingga kamu tidak perlu menanyakan hal yang sama.
            Harap memberi pertanyaan yang membutuhkan jawaban "Ya" atau "Tidak" dan <u>baca soal dengan seksama</u> sebelum meminta klarifikasi.
            </p>
        </li>
	<!--
        <li><strong>Peringkat</strong>
            <p>Bagian ini menampilkan peringkat peserta kontes berdasarkan nilai yang diberikan untuk jawaban peserta.
                <u>Usahakan untuk meraih peringkat teratas!</u></p>
        </li>
	-->
    </ol>
</div>
