<?php

return array(
    'compiler' => array(
        'temp_dir' => "/var/www/html/lz/temp"
    ),
    'evaluator' => array(
        'problem' => array(
            'problem_repository_path' => '/var/tokilx/prod/problems',
            'problem_type_repository_path' => '/var/tokilx/prod/problem_types'
        ),
        'compiler' => array(
            'compiler_gcc_path' => '/usr/bin/gcc',
            'compiler_cpp_path' => '/usr/bin/g++',
            'compiler_fpc_path' => '/usr/bin/fpc',
        ),
        'sandbox' => array(
            'moevalbox_path' => '/bin/mo-evalbox',
            'moeval_iwrapper_path' => '/bin/mo-eval-iwrapper',
            'sandbox_temp_dir' => '/var/tokilx/prod/tmp'
        ),
        'balancer' => array(
            'message-key' => '2178',
            'message-type' => '10',
            'config-file' => '/etc/balancer.conf',
            'pid-file' => '/etc/balancer-lx2.pid',
            'app-id' => 'lx2'
        )
    )
);
