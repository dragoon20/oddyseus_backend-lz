<?php

return array(
    'evaluator' => array(
        'problem' => array(
            'problem_repository_path' => '/var/tokilx/dev/problems',
            'problem_type_repository_path' => '/var/tokilx/dev/problem_types'
        ),
        'compiler' => array(
            'compiler_gcc_path' => '/usr/bin/gcc',
            'compiler_cpp_path' => '/usr/bin/g++',
            'compiler_fpc_path' => '/usr/bin/fpc',
        ),
        'sandbox' => array(
            'moevalbox_path' => '/bin/mo-evalbox',
            'sandbox_temp_dir' => '/var/tokilx/dev/tmp'
        )
    )
);


