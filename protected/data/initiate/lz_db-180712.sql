-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 18, 2012 at 10:17 PM
-- Server version: 5.5.20
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lz_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE IF NOT EXISTS `announcements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `arenas`
--

CREATE TABLE IF NOT EXISTS `arenas` (
  `id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(14) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `arenas`
--

INSERT INTO `arenas` (`id`, `name`, `status`, `creator_id`) VALUES
(5, 'Pelatnas 1 TOKI 2012', 0, 8),
(6, 'TOKI Training Gate', 0, 8),
(7, 'Akses Publik TOKI', 0, 8),
(8, 'Open Contest Archive', 0, 22);

-- --------------------------------------------------------

--
-- Table structure for table `arenas_problems`
--

CREATE TABLE IF NOT EXISTS `arenas_problems` (
  `arena_id` int(14) unsigned NOT NULL,
  `problem_id` int(14) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `arenas_users`
--

CREATE TABLE IF NOT EXISTS `arenas_users` (
  `arena_id` int(14) unsigned NOT NULL,
  `user_id` int(14) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `arenas_users`
--

INSERT INTO `arenas_users` (`arena_id`, `user_id`) VALUES
(5, 5),
(5, 1291),
(5, 10),
(5, 1214),
(5, 22),
(5, 989),
(6, 1291),
(6, 1293),
(5, 968),
(5, 3998),
(5, 798),
(5, 1159),
(5, 1222),
(5, 1265),
(5, 1534),
(5, 1541),
(5, 1777),
(5, 4190),
(5, 682),
(5, 1211),
(5, 1212),
(5, 1217),
(5, 1223),
(5, 1342),
(5, 1),
(6, 10),
(7, 1293),
(7, 1291),
(7, 10),
(7, 22),
(8, 10),
(8, 1293),
(8, 1291),
(8, 1214),
(7, 1222),
(7, 6),
(7, 1214),
(6, 1222),
(6, 6),
(6, 1214),
(6, 1459),
(6, 1362),
(6, 1414),
(6, 4190);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `contest_id` int(10) unsigned DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `contest_id` (`contest_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AuthAssignment`
--

CREATE TABLE IF NOT EXISTS `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AuthAssignment`
--

INSERT INTO `AuthAssignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('administrator', '8', NULL, 'N;'),
('administrator', '1', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `AuthItem`
--

CREATE TABLE IF NOT EXISTS `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AuthItem`
--

INSERT INTO `AuthItem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('administrator', 2, 'Administrator', NULL, 'N;'),
('supervisor', 2, 'Supervisor', NULL, 'N;'),
('learner', 2, 'Learner', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `AuthItemChild`
--

CREATE TABLE IF NOT EXISTS `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE IF NOT EXISTS `chapters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_time` datetime NOT NULL,
  `next_chapter_id` int(10) DEFAULT NULL,
  `first_subchapter_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `next_chapter_id` (`next_chapter_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `name`, `description`, `created_time`, `next_chapter_id`, `first_subchapter_id`) VALUES
(7, 'Bab 1 : Pengenalan', '<p>\r\n	Bab ini akan membahas dasar-dasar pemrograman, khususnya menggunakan bahasa Pascal. Oleh karena itu, unduhlah terlebih dahulu kompilator Pascal yang umum digunakan, yaitu <a href="http://freepascal.org/download.var">Free Pascal</a>.</p>\r\n<p>\r\n	Sebagai perkenalan pertama dengan program Pascal, ketikanlah kode-kode berikut ini dalam berkas bernama &#39;pertamaku.pas&#39;.</p>\r\n<pre>program pertamaku;\r\nvar\r\n    kalimat : string;\r\nbegin\r\n    readln(kalimat);\r\n    writeln(kalimat);\r\nend.\r\n</pre>\r\n<p>\r\n	Program ini akan membaca satu baris teks masukan (dari <em>standard input</em>, yaitu keyboard) dan mencetak keluaran (ke <em>standard output,</em> yaitu layar monitor) yang persis sama dengan masukan.</p>\r\n<p>\r\n	Pada bagian awal, terdapat pernyataan <strong>program pertamaku;</strong> yang menandakan bahwa program ini bernama &#39;pertamaku&#39;. Pernyataan ini bersifat opsional, yaitu boleh ada boleh tidak.</p>\r\n<p>\r\n	Selanjutnya, terdapat deklarasi yang menyebutkan digunakannya suatu variabel dengan nama <b>kalimat</b>. Deklarasi ditunjukkan dengan adanya notasi <b>var</b>. Baris-baris berikutnya setelah notasi <b>var</b> adalah tempat menuliskan deklarasi variabel-variabel, dalam format</p>\r\n<pre>&lt;nama-variabel&gt; : &lt;tipe-variabel&gt;;</pre>\r\n<p>\r\n	Variabel adalah tempat menyimpan suatu nilai dalam program, dan selama berjalannya program, nilai itu dapat berubah-ubah. Setiap variabel dideklarasikan dengan menyebutkan tipe dari nilai yang dapat disimpannya. <b><span class="b">kalimat</span></b> dideklarasikan sebagai variabel berjenis <b>string</b>, berarti <b>kalimat</b> dapat menyimpan string, yaitu rangkaian karakter, yang panjangnya maksimum 255 karakter.</p>\r\n<p>\r\n	Badan program dinyatakan dengan perintah <b>begin</b> dan diakhiri dengan perintah <b>end.</b>, yaitu <b>end</b> dengan tanda titik. <b>readln(kalimat)</b> berfungsi untuk membaca satu baris string masukan dan hasil pembacaannya disimpan dalam variabel <b>kalimat</b>. Perintah <b>writeln(kalimat)</b> berguna untuk menuliskan isi variabel <b>kalimat</b> ke keluaran. Dalam program, setiap perintah diakhiri dengan tanda <b>;</b> (titik koma).</p>\r\n<p>\r\n	Untuk menguji program Anda, bukalah penyunting teks (Notepad atau yang lainnya) lalu ketikan teks sesuka anda dalam satu baris dengan panjang tidak lebih dari 100 karakter. Simpanlah teks tersebut dalam berkas teks, misalnya dengan nama &#39;uji.txt&#39;. Kompilasi program tersebut dengan Free Pascal menjadi &#39;bebek.exe&#39;, lalu jalankan perintah pada <em>command prompt</em>:</p>\r\n<pre>bebek &lt; uji.txt</pre>\r\n<p>\r\n	Jika program mengeluarkan keluaran yang sama dengan isi teks pada uji.txt, maka program Anda sudah berjalan dengan benar.</p>\r\n<p>\r\n	Selesailah program pertama Anda! Sebagai bonus, berkas ini adalah (salah satu) jawaban dari soal Program Pertamaku pada Bab 1A. Silakan coba mengumpulkannya. Ingat, yang dikumpulkan adalah berkas <em>.pas</em>-nya, bukan <em>.exe</em>-nya.</p>\r\n<p>\r\n	Sekali lagi, selamat berlatih!</p>\r\n', '2011-01-22 22:54:31', 8, 9),
(8, 'Bab 2 : Struktur Data', '<p>\r\n	Struktur Data</p>\r\n', '2011-01-22 22:54:47', 17, 12),
(9, '1A : Masukan dan Keluaran', '<p>\r\n	Pada seluruh soal pada Training Gate ini, masukan diambil dari <em>standard input</em> yaitu keyboard, sedangkan keluaran menuju <em>standard output</em> yaitu layar monitor.</p>\r\n<h3>\r\n	<strong>read </strong>&amp;<strong> readln<br />\r\n	</strong></h3>\r\n<p>\r\n	Perintah ini berfungsi untuk membaca masukan dari keyboard dan menyimpannya dalam suatu variabel. Bedanya adalah, setelah membaca, <strong>readln</strong> akan menyebabkan pembacaan berpindah ke baris berikutnya, sedangkan <strong>read</strong> tidak.</p>\r\n<p>\r\n	Misalnya, untuk membaca sebuah bilangan bulat dalam suatu baris dan menyimpannya ke dalam variabel <strong>bil</strong>, gunakan perintah</p>\r\n<pre>readln(bil);\r\n</pre>\r\n<p>\r\n	Untuk membaca 2 atau lebih masukan dalam satu baris yang dipisahkan oleh spasi, tambahkan variabel penampungnya ke dalam perintah <strong>read</strong>/<strong>readln</strong>, dipisahkan oleh koma. Misalnya, untuk membaca tiga buah bilangan bulat dalam satu baris ke dalam tiga buah variabel <strong>bil1</strong>, <strong>bil2</strong>, dan <strong>bil3</strong>, gunakan perintah</p>\r\n<pre>readln(bil1, bil2, bil3);\r\n</pre>\r\n<p>\r\n	Perintah tersebut sama dengan</p>\r\n<pre>read(bil1); read(bil2); readln(bil3);\r\n</pre>\r\n<h3>\r\n	<strong>write </strong>&amp;<strong> writeln</strong></h3>\r\n<p>\r\n	Perintah ini berfungsi untuk mencetak suatu nilai ke layar monitor. Bedanya adalah, setelah mencetak, <strong>writeln</strong> akan menyebabkan pencetakan berpindah ke baris berikutnya, sedangkan <strong>write</strong> tidak.</p>\r\n<p>\r\n	Misalnya, untuk mencetak isi dari variabel <strong>bil</strong> dalam sebuah baris ke layar, gunakan perintah</p>\r\n<pre>writeln(bil);\r\n</pre>\r\n<p>\r\n	Sama seperti <strong>read</strong>/<strong>readln</strong>, untuk mencetak 2 atau lebih nilai pada sebuah baris, tambahkan nilai-nilainya ke dalam perintah tersebut, dipisahkan oleh koma. Misalnya, untuk mencetak isi tiga buah variabel <strong>bil1</strong>, <strong>bil2</strong>, dan <strong>bil3</strong>, dipisahkan oleh spasi, gunakan perintah</p>\r\n<pre>writeln(bil1, &#39; &#39;, bil2, &#39; &#39;, bil3);\r\n</pre>\r\n<p>\r\n	Nilai yang berada di dalam perintah <strong>write</strong>/<strong>writeln</strong> tidak harus berupa variabel; bisa pula berupa konstanta. Misalnya, Anda dapat memberikan perintah</p>\r\n<pre>writeln(&#39;Selamat datang, &#39;, nama, &#39;!&#39;);\r\n</pre>\r\n<p>\r\n	Konstanta berupa string pada Pascal diapit dengan tanda petik satu.</p>\r\n<h3>\r\n	<strong>Tipe-Tipe Data Utama pada Free Pascal</strong></h3>\r\n<table style="width:450px;">\r\n	<tbody>\r\n		<tr>\r\n			<th>\r\n				Nama</th>\r\n			<th>\r\n				Jenis</th>\r\n			<th>\r\n				Jangkauan</th>\r\n			<th>\r\n				Ukuran</th>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				integer</td>\r\n			<td>\r\n				bilangan bulat</td>\r\n			<td>\r\n				-2<sup>15</sup> sampai 2<sup>15</sup>-1</td>\r\n			<td>\r\n				2 byte</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				longint</td>\r\n			<td>\r\n				bilangan bulat</td>\r\n			<td>\r\n				-2<sup>31</sup> sampai 2<sup>31</sup>-1</td>\r\n			<td>\r\n				4 byte</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				int64</td>\r\n			<td>\r\n				bilangan bulat</td>\r\n			<td>\r\n				-2<sup>63</sup> sampai 2<sup>63</sup>-1</td>\r\n			<td>\r\n				8 byte</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				real</td>\r\n			<td>\r\n				bilangan nyata</td>\r\n			<td>\r\n				-2,2 x 10<sup>308</sup> sampai 1,7 x 10<sup>308</sup></td>\r\n			<td>\r\n				8 byte</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				boolean</td>\r\n			<td>\r\n				logika Boolean</td>\r\n			<td>\r\n				{false, true}</td>\r\n			<td>\r\n				1 byte</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				char</td>\r\n			<td>\r\n				karakter</td>\r\n			<td>\r\n				1 karakter ASCII</td>\r\n			<td>\r\n				1 byte</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				string</td>\r\n			<td>\r\n				rangkaian karakter</td>\r\n			<td>\r\n				0-255 karakter ASCII</td>\r\n			<td>\r\n				n byte</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<h3>\r\n	<strong>Memberi Nilai Langsung pada Variabel </strong></h3>\r\n<p>\r\n	Sejauh ini kita telah mempelajari cara memberikan nilai pada variabel lewat masukan yang diberikan pengguna. Kita juga bisa langsung memberi nilai pada variabel, dengan operator &#39;:=&#39;. Nilai yang berada di sebelah kanan operator ini akan dimasukkan ke dalam variabel yang berada di sebelah kiri operator ini. Misalnya,</p>\r\n<pre>hasil := 5;\r\n</pre>\r\n<p>\r\n	Sekarang variabel <strong>hasil</strong> akan berisi nilai 5.</p>\r\n', '2011-01-22 22:57:19', 10, NULL),
(10, '1B : Percabangan', '<p>\r\n	Subbab ini membahas percabangan.</p>\r\n', '2011-01-22 22:57:38', 13, NULL),
(11, '2B : Array Multidimensi', 'Mengenal Array Multidimensi', '2011-01-22 22:57:54', 15, NULL),
(12, '2A : Array', '<p>\r\n	Mengenal Array satu dimensi</p>\r\n', '2011-01-22 22:58:42', 11, NULL),
(13, '1C : Perulangan', 'Mengenal sintaks operasi perulangan', '2011-01-25 22:40:51', 14, NULL),
(14, '1D : Fungsi dan Prosedur', 'Mengenal subrutin fungsi dan prosedur', '2011-01-25 22:43:08', NULL, NULL),
(15, '2C : String', 'Mengenal struktur data string dan manipulasinya', '2011-01-25 23:31:02', 16, NULL),
(16, '2D : Stack dan Queue', 'Mengenal struktur data stack dan queue', '2011-01-25 23:32:37', NULL, NULL),
(17, 'Bab 3 : Ad Hoc', '<p>\r\n	Latihan menyelesaikan masalah-masalah trivia dengan bekal materi pada chapter sebelumnya.</p>\r\n', '2011-01-26 22:15:07', 22, 18),
(18, '3A : Simulasi', 'Melatih kemampuan mensimulasikan masalah dengan program', '2011-01-26 22:23:20', 19, NULL),
(19, '3B : Pengurutan', '<p>\r\n	Beberapa testcase masih bermasalah!</p>\r\n', '2011-01-26 22:23:54', 20, NULL),
(20, '3C : Teori Bilangan', 'Kumpulan latihan dengan tema matematika sederhana', '2011-01-26 22:26:14', NULL, NULL),
(21, 'TOKI Training Gate - BETA', '<div>\r\n	Selamat datang di TOKI Training Gate - BETA! Sesuai namanya, laman ini belum final dan masih dalam tahap pengembangan dan pengujian. Jika Anda menemukan bug atau ingin memberi saran, silakan layangkan di <a href="http://www.facebook.com/home.php?sk=group_166544345960&amp;view=doc&amp;id=10150275830145961">halaman ini</a>.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Silakan mengerjakan soal-soal di sini sambil melihat deskripsi soalnya apakah masih ambigu, kesalahan testcase, atau kekurangan lainnya. Umpan balik Anda sangat diharapkan.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Selamat berlatih!</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	<em>TOKI LC Team. Viva TOKI, Go Get Golds!</em></div>\r\n', '2011-01-27 08:39:20', NULL, 7),
(22, 'Bab 4 : Complete Search', '<p>\r\n	Rekursif dan Pencarian Menyeluruh</p>\r\n', '2011-01-29 19:31:22', 27, 23),
(23, '4A : Complete Search I', 'Mengenal Problem Rekursif Dasar', '2011-01-29 19:32:08', 24, NULL),
(24, '4B : Complete Search II', 'Permutasi, Kombinasi, dan Backtracking', '2011-01-29 19:32:50', 25, NULL),
(25, '4C : Complete Search III', 'Breadth First Search dan Depth First Search', '2011-01-29 19:34:15', 26, NULL),
(26, '4D : Analisis Algoritma', '<p>\r\n	Melatih menganalisa kompleksitas algoritma serta efisiensi waktu dan memori</p>\r\n', '2011-01-29 19:35:29', NULL, NULL),
(27, 'Bab 5 : Divide-and-Conquer', '<p>\r\n	Mengenal teknik pemrograman Divide and Conquer</p>\r\n', '2011-01-29 19:57:38', 32, 28),
(28, '5A : Dichotomic Search', '<p>\r\n	Mengenal pencarian dikotomik</p>\r\n', '2011-01-29 19:59:46', 29, NULL),
(29, '5B : Binary Search I', '<p>\r\n	Mengenal teknik binary search sebagai kasus khusus dari dichotomic search</p>\r\n', '2011-01-29 20:01:18', 30, NULL),
(30, '5C : Binary Search II', '<p>\r\n	Memanipulasi teknik binary search untuk memecahan masalah</p>\r\n', '2011-01-29 20:02:31', 31, NULL),
(31, '5D : Merge dan Quick Sort', 'Mengenal algoritma pengurutan Merge dan Quick', '2011-01-29 20:03:13', NULL, NULL),
(32, 'Bab 6 : Dynamic Programming & Greedy', '<p>\r\n	Mengenal konsep Dynamic Programming dan Greedy</p>\r\n', '2011-01-29 20:17:09', 35, 33),
(33, '6A : Greedy', '<p>\r\n	Mengenal teknik pemrograman Greedy</p>\r\n', '2011-01-29 20:18:00', 34, NULL),
(34, '6B : Dynamic Programming', '<p>\r\n	Mengenal teknik pemrograman Dynamic Programming</p>\r\n', '2011-01-29 20:18:41', NULL, NULL),
(35, 'Bab 7 : Graf', '<p>\r\n	Mengenal masalah-masalah jaringan/graph.</p>\r\n', '2011-01-29 20:32:30', 39, 36),
(36, '7A : Traversal', 'Mengenal tentang representasi graph pada struktur data dan traversalnya', '2011-01-29 20:35:47', 37, NULL),
(37, '7B : Shortest Path', 'Graph dalam kasus jarak terpendek', '2011-01-29 20:37:22', 38, NULL),
(38, '7C : Minimum Spanning Tree', 'Membahas graph dalam kasus pohon bentangan minimal serta tree sebagai bentuk khusus dari graph', '2011-01-29 20:38:43', NULL, NULL),
(39, 'Bab 8 : Soal-Soal Menantang!', '<p>\r\n	Membahas soal-soal dengan tingkat kesulitan yang cukup tinggi seperti geometri, matematika lanjut dan NP Complete Problems</p>\r\n', '2011-01-29 20:44:48', NULL, 40),
(40, '8A : Matematika', 'Membahas soal yang membutuhkan pengetahuan matematika yang cukup kuat.', '2011-01-29 20:46:19', 41, NULL),
(41, '8B : Geometri', 'Membahas soal yang membutuhkan pengetahuan geometri yang cukup kuat', '2011-01-29 20:47:02', 42, NULL),
(42, '8C : NP Complete', 'NP Complete Problems', '2011-01-29 20:47:40', NULL, NULL),
(43, '1. Tes', '<p>\r\n	1. tes</p>\r\n<h1>\r\n	MUHAHAHHAA</h1>\r\n', '2011-02-17 12:45:33', 45, 48),
(44, 'Tes', '<p>\r\n	<b>Selamat Datang.</b></p>\r\n', '2011-02-17 12:46:17', NULL, 43),
(45, 'bab 2 tes', '<p>\r\n	2. tes</p>\r\n', '2011-02-17 12:46:27', NULL, NULL),
(48, 'c-c', '<p>\r\n	a</p>\r\n', '2012-01-27 22:48:37', 49, NULL),
(49, 'd', '1', '2012-01-27 22:49:33', 50, NULL),
(50, 'e', 'ee', '2012-01-27 23:16:14', NULL, NULL),
(46, 'coba 1 - bab 1', '<p>\r\n	<tes> <b>Ini merupakan deskripsi bab 1</b></tes></p>\r\n', '2012-01-27 21:41:57', NULL, 47),
(47, 'coba 1 - bab 1 - subbab 1', '<had', '2012-01-27 21:44:27', 47, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chapters_problems`
--

CREATE TABLE IF NOT EXISTS `chapters_problems` (
  `chapter_id` int(10) unsigned NOT NULL,
  `problem_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chapters_users`
--

CREATE TABLE IF NOT EXISTS `chapters_users` (
  `chapter_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `status` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clarifications`
--

CREATE TABLE IF NOT EXISTS `clarifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contest_id` int(10) unsigned DEFAULT NULL,
  `chapter_id` int(14) unsigned DEFAULT NULL,
  `problem_id` int(10) unsigned DEFAULT NULL,
  `questioner_id` int(11) unsigned NOT NULL,
  `questioned_time` datetime NOT NULL,
  `subject` text COLLATE latin1_general_ci NOT NULL,
  `question` text COLLATE latin1_general_ci NOT NULL,
  `answerer_id` int(11) unsigned DEFAULT NULL,
  `answered_time` datetime DEFAULT NULL,
  `answer` text COLLATE latin1_general_ci,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contest_id` (`contest_id`),
  KEY `questioner_id` (`questioner_id`),
  KEY `answerer_id` (`answerer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contestnews`
--

CREATE TABLE IF NOT EXISTS `contestnews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `contest_id` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `contest_id` (`contest_id`),
  KEY `author_id_2` (`author_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contests`
--

CREATE TABLE IF NOT EXISTS `contests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `contest_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'open, closed, hidden',
  `invitation_type` int(11) NOT NULL DEFAULT '0',
  `configuration` text,
  `span_type` int(5) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contests_problems`
--

CREATE TABLE IF NOT EXISTS `contests_problems` (
  `contest_id` int(11) NOT NULL,
  `alias` int(11) NOT NULL,
  `problem_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`contest_id`,`problem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contests_users`
--

CREATE TABLE IF NOT EXISTS `contests_users` (
  `contest_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `status` int(11) unsigned NOT NULL,
  `role` int(11) NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `last_page` text,
  PRIMARY KEY (`contest_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contest_logs`
--

CREATE TABLE IF NOT EXISTS `contest_logs` (
  `id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `contest_id` int(14) unsigned NOT NULL,
  `actor_id` int(14) NOT NULL,
  `action_type` int(11) NOT NULL,
  `time` int(20) NOT NULL,
  `log_content` text,
  PRIMARY KEY (`id`),
  KEY `time` (`time`),
  KEY `contest_id` (`contest_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contest_types`
--

CREATE TABLE IF NOT EXISTS `contest_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contest_types`
--

INSERT INTO `contest_types` (`id`, `name`, `description`) VALUES
(1, 'ioi', 'Standard contest for International Olympiads in Informatics'),
(2, 'acmicpc', 'Standard contest for ACM ICPC'),
(3, 'ioi2010', 'ioi2010');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_sets`
--

CREATE TABLE IF NOT EXISTS `evaluation_sets` (
  `id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(14) unsigned DEFAULT NULL,
  `config` text,
  `grader_type_id` varchar(255) DEFAULT NULL,
  `compiler_type_id` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `evaluation_sets`
--

INSERT INTO `evaluation_sets` (`id`, `client_id`, `config`, `grader_type_id`, `compiler_type_id`, `date_created`, `file`) VALUES
(9, 1, '{"testerfilename":"test-do.cpp","new":""}', 'simple', 'archive', '2012-07-06 08:39:07', 0x504b03040a00000000002c74eb401608261a020000000200000005001c00322e6f75745554090003432cfd4f432cfd4f75780b000104e803000004e8030000340a504b03040a00000000002b74eb408fd7547b040000000400000004001c00322e696e5554090003412cfd4f412cfd4f75780b000104e803000004e8030000320a320a504b03040a00000000002974eb40d19e6755020000000200000005001c00312e6f757455540900033e2cfd4f3e2cfd4f75780b000104e803000004e8030000330a504b03040a00000000002874eb406178e169040000000400000004001c00312e696e55540900033c2cfd4f3c2cfd4f75780b000104e803000004e8030000310a320a504b01021e030a00000000002c74eb401608261a0200000002000000050018000000000001000000b48100000000322e6f75745554050003432cfd4f75780b000104e803000004e8030000504b01021e030a00000000002b74eb408fd7547b0400000004000000040018000000000001000000b48141000000322e696e5554050003412cfd4f75780b000104e803000004e8030000504b01021e030a00000000002974eb40d19e67550200000002000000050018000000000001000000b48183000000312e6f757455540500033e2cfd4f75780b000104e803000004e8030000504b01021e030a00000000002874eb406178e1690400000004000000040018000000000001000000b481c4000000312e696e55540500033c2cfd4f75780b000104e803000004e8030000504b050600000000040004002a010000060100000000),
(10, 1, '{"new":""}', 'batch', 'archive', '2012-07-11 07:34:28', 0x504b03040a0000000000007aeb4057393d03020000000200000007001c00332e73636f726555540900033f36fd4f3f36fd4f75780b000104e803000004e8030000350a504b03040a0000000000f079eb40d55b0b31020000000200000005001c00332e6f757455540900032436fd4f3536fd4f75780b000104e803000004e8030000370a504b03040a0000000000f279eb406178e169040000000400000004001c00332e696e55540900032836fd4f3536fd4f75780b000104e803000004e8030000310a320a504b03040a0000000000fc79eb4044c34a9a030000000300000007001c00322e73636f726555540900033c36fd4f3c36fd4f75780b000104e803000004e803000032350a504b03040a00000000002c74eb401608261a020000000200000005001c00322e6f75745554090003432cfd4f4f2cfd4f75780b000104e803000004e8030000340a504b03040a00000000002b74eb408fd7547b040000000400000004001c00322e696e5554090003412cfd4f4f2cfd4f75780b000104e803000004e8030000320a320a504b03040a0000000000d979eb40eaf5f6e1030000000300000007001c00312e73636f72655554090003f935fd4f0b36fd4f75780b000104e803000004e803000037300a504b03040a00000000002974eb40d19e6755020000000200000005001c00312e6f757455540900033e2cfd4f4f2cfd4f75780b000104e803000004e8030000330a504b03040a00000000002874eb406178e169040000000400000004001c00312e696e55540900033c2cfd4f4f2cfd4f75780b000104e803000004e8030000310a320a504b01021e030a0000000000007aeb4057393d030200000002000000070018000000000001000000b48100000000332e73636f726555540500033f36fd4f75780b000104e803000004e8030000504b01021e030a0000000000f079eb40d55b0b310200000002000000050018000000000001000000b48143000000332e6f757455540500032436fd4f75780b000104e803000004e8030000504b01021e030a0000000000f279eb406178e1690400000004000000040018000000000001000000b48184000000332e696e55540500032836fd4f75780b000104e803000004e8030000504b01021e030a0000000000fc79eb4044c34a9a0300000003000000070018000000000001000000b481c6000000322e73636f726555540500033c36fd4f75780b000104e803000004e8030000504b01021e030a00000000002c74eb401608261a0200000002000000050018000000000001000000b4810a010000322e6f75745554050003432cfd4f75780b000104e803000004e8030000504b01021e030a00000000002b74eb408fd7547b0400000004000000040018000000000001000000b4814b010000322e696e5554050003412cfd4f75780b000104e803000004e8030000504b01021e030a0000000000d979eb40eaf5f6e10300000003000000070018000000000001000000b4818d010000312e73636f72655554050003f935fd4f75780b000104e803000004e8030000504b01021e030a00000000002974eb40d19e67550200000002000000050018000000000001000000b481d1010000312e6f757455540500033e2cfd4f75780b000104e803000004e8030000504b01021e030a00000000002874eb406178e1690400000004000000040018000000000001000000b48112020000312e696e55540500033c2cfd4f75780b000104e803000004e8030000504b05060000000009000900a6020000540200000000);

-- --------------------------------------------------------

--
-- Table structure for table `grade_requests`
--

CREATE TABLE IF NOT EXISTS `grade_requests` (
  `id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(14) unsigned DEFAULT NULL,
  `evaluationset_id` int(14) unsigned DEFAULT NULL,
  `submitter_id` varchar(255) DEFAULT NULL,
  `submitted_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `receive_time` timestamp NULL DEFAULT NULL,
  `dispatch_time` timestamp NULL DEFAULT NULL,
  `graded_time` timestamp NULL DEFAULT NULL,
  `report_time` timestamp NULL DEFAULT NULL,
  `source_file` varchar(255) DEFAULT NULL,
  `action` int(11) NOT NULL,
  `report` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `mode` int(11) NOT NULL DEFAULT '0',
  `request_id` int(14) unsigned DEFAULT NULL,
  `file` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `grade_requests`
--

INSERT INTO `grade_requests` (`id`, `client_id`, `evaluationset_id`, `submitter_id`, `submitted_time`, `receive_time`, `dispatch_time`, `graded_time`, `report_time`, `source_file`, `action`, `report`, `status`, `mode`, `request_id`, `file`) VALUES
(7, 1, 9, 'Test Moodle', '2012-07-11 06:27:46', '2012-07-11 07:22:08', '2012-07-11 07:22:10', '2012-07-11 07:22:10', '2012-07-11 07:22:10', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/7''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/7\\/Main test-do.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/7''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/7''\\n\\/var\\/lz\\/temp\\/7\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/7''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.008 sec wall, 0 syscalls)"}}', 2, 0, 238, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(8, 1, 9, 'Test Moodle', '2012-07-11 06:27:51', '2012-07-11 07:19:26', '2012-07-11 07:19:02', '2012-07-11 07:19:02', '2012-07-11 07:19:02', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/8''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/8\\/Main test-do.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/8''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/8''\\n\\/var\\/lz\\/temp\\/8\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/8''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.014 sec wall, 0 syscalls)"}}', 3, 0, 239, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(9, 1, 9, 'Test Moodle', '2012-07-11 06:27:56', '2012-07-11 07:19:26', '2012-07-11 07:19:01', '2012-07-11 07:19:01', '2012-07-11 07:19:01', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/9''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/9\\/Main test-do.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/9''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/9''\\n\\/var\\/lz\\/temp\\/9\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/9''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.019 sec wall, 0 syscalls)"}}', 3, 0, 240, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(10, 1, 9, 'Test Moodle', '2012-07-11 06:28:01', '2012-07-11 07:19:26', '2012-07-11 07:19:02', '2012-07-11 07:19:02', '2012-07-11 07:19:02', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/10''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/10\\/Main test-do.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/10''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/10''\\n\\/var\\/lz\\/temp\\/10\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/10''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.035 sec wall, 0 syscalls)"}}', 3, 0, 241, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(11, 1, 9, 'Test Moodle', '2012-07-11 06:28:06', '2012-07-11 07:19:26', '2012-07-11 07:19:01', '2012-07-11 07:19:01', '2012-07-11 07:19:01', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/11''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/11\\/Main test-do.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/11''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/11''\\n\\/var\\/lz\\/temp\\/11\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/11''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.057 sec wall, 0 syscalls)"}}', 3, 0, 242, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(12, 1, 9, 'Test Moodle', '2012-07-11 06:28:11', '2012-07-11 07:19:26', '2012-07-11 07:19:02', '2012-07-11 07:19:02', '2012-07-11 07:19:02', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/12''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/12\\/Main test-do.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/12''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/12''\\n\\/var\\/lz\\/temp\\/12\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/12''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.032 sec wall, 0 syscalls)"}}', 3, 0, 243, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(13, 1, 9, 'Test Moodle', '2012-07-11 06:28:16', '2012-07-11 07:19:26', '2012-07-11 07:19:02', '2012-07-11 07:19:02', '2012-07-11 07:19:02', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/13''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/13\\/Main test-do.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/13''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/13''\\n\\/var\\/lz\\/temp\\/13\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/13''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.008 sec wall, 0 syscalls)"}}', 3, 0, 244, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(14, 1, 10, 'Test Moodle', '2012-07-11 07:48:33', '2012-07-11 07:48:33', '2012-07-11 07:50:47', '2012-07-11 07:50:47', '2012-07-11 07:50:47', 'batch.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/14''\\nmake: \\/var\\/lz\\/temp\\/14\\/Makefile: No such file or directory\\nmake: *** No rule to make target `\\/var\\/lz\\/temp\\/14\\/Makefile''.  Stop.\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/14''","return":2},"grade_result":null}', 2, 0, 251, 0x504b03040a00000000002c74eb401608261a020000000200000005001c00322e6f75745554090003432cfd4f432cfd4f75780b000104e803000004e8030000340a504b03040a00000000002b74eb408fd7547b040000000400000004001c00322e696e5554090003412cfd4f412cfd4f75780b000104e803000004e8030000320a320a504b03040a00000000002974eb40d19e6755020000000200000005001c00312e6f757455540900033e2cfd4f3e2cfd4f75780b000104e803000004e8030000330a504b03040a00000000002874eb406178e169040000000400000004001c00312e696e55540900033c2cfd4f3c2cfd4f75780b000104e803000004e8030000310a320a504b01021e030a00000000002c74eb401608261a0200000002000000050018000000000001000000b48100000000322e6f75745554050003432cfd4f75780b000104e803000004e8030000504b01021e030a00000000002b74eb408fd7547b0400000004000000040018000000000001000000b48141000000322e696e5554050003412cfd4f75780b000104e803000004e8030000504b01021e030a00000000002974eb40d19e67550200000002000000050018000000000001000000b48183000000312e6f757455540500033e2cfd4f75780b000104e803000004e8030000504b01021e030a00000000002874eb406178e1690400000004000000040018000000000001000000b481c4000000312e696e55540500033c2cfd4f75780b000104e803000004e8030000504b050600000000040004002a010000060100000000),
(15, 1, 10, 'Test Moodle', '2012-07-11 07:51:24', '2012-07-11 07:51:24', '2012-07-11 08:22:01', '2012-07-11 08:22:01', '2012-07-11 08:22:01', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/15''\\ng++ -o \\/var\\/lz\\/temp\\/15\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/15''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/15\\/tc\\/1.in":"Correct\\nOK (0.001 sec real, 0.003 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/15\\/tc\\/2.in":"Correct\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/15\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 252, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(16, 1, 10, 'Test Moodle', '2012-07-11 08:20:35', '2012-07-11 08:26:19', '2012-07-11 08:37:06', '2012-07-11 08:37:06', '2012-07-11 08:37:06', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/16''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/16\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/16''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/16\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/16\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/16\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 254, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(17, 1, 10, 'Test Moodle', '2012-07-11 08:24:08', '2012-07-11 08:26:19', '2012-07-11 08:37:04', '2012-07-11 08:37:04', '2012-07-11 08:37:04', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/17''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/17\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/17''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/17\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/17\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/17\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.005 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.005 sec wall, 0 syscalls)"}}', 2, 0, 255, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(18, 1, 10, 'Test Moodle', '2012-07-11 08:25:05', '2012-07-11 08:26:19', '2012-07-11 08:37:01', '2012-07-11 08:37:01', '2012-07-11 08:37:01', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/18''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/18\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/18''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/18\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/18\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/18\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 256, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(19, 1, 9, 'Test Moodle', '2012-07-11 08:25:06', '2012-07-11 08:26:19', '2012-07-11 08:36:57', '2012-07-11 08:36:57', '2012-07-11 08:36:57', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/19''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/19\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/19''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/19''\\n.\\/\\/var\\/lz\\/temp\\/19\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/19\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/19''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 257, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(20, 1, 10, 'Test Moodle', '2012-07-11 08:25:06', '2012-07-11 08:26:19', '2012-07-11 08:36:54', '2012-07-11 08:36:54', '2012-07-11 08:36:54', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/20''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/20\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/20''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/20\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/20\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.005 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/20\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 258, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(21, 1, 10, 'Test Moodle', '2012-07-11 08:37:45', '2012-07-11 08:37:45', '2012-07-11 08:56:32', '2012-07-11 08:56:32', '2012-07-11 08:56:32', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/21''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/21\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/21''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/21\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/21\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/21\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 284, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(22, 1, 10, 'Test Moodle', '2012-07-11 08:55:12', '2012-07-11 08:55:12', '2012-07-11 08:56:36', '2012-07-11 08:56:36', '2012-07-11 08:56:36', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/22''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/22\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/22''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/22\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/22\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/22\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 285, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(23, 1, 10, 'Test Moodle', '2012-07-11 08:55:56', '2012-07-11 08:55:56', '2012-07-11 08:56:40', '2012-07-11 08:56:40', '2012-07-11 08:56:40', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/23''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/23\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/23''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/23\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/23\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/23\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 286, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(24, 1, 9, 'Test Moodle', '2012-07-11 08:55:56', '2012-07-11 08:55:56', '2012-07-11 08:56:42', '2012-07-11 08:56:42', '2012-07-11 08:56:42', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/24''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/24\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/24''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/24''\\n.\\/\\/var\\/lz\\/temp\\/24\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/24\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/24''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 287, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000);
INSERT INTO `grade_requests` (`id`, `client_id`, `evaluationset_id`, `submitter_id`, `submitted_time`, `receive_time`, `dispatch_time`, `graded_time`, `report_time`, `source_file`, `action`, `report`, `status`, `mode`, `request_id`, `file`) VALUES
(25, 1, 10, 'Test Moodle', '2012-07-11 08:55:57', '2012-07-11 08:55:57', '2012-07-11 08:57:12', '2012-07-11 08:57:12', '2012-07-11 08:57:12', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/25''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/25\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/25''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/25\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/25\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/25\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 288, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(26, 1, 10, 'Test Moodle', '2012-07-11 08:59:43', '2012-07-11 08:59:43', '2012-07-11 09:00:57', '2012-07-11 09:00:57', '2012-07-11 09:00:57', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/26''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/26\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/26''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/26\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/26\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/26\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.003 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.003 sec wall, 0 syscalls)"}}', 2, 0, 292, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(27, 1, 10, 'Test Moodle', '2012-07-11 09:00:37', '2012-07-11 09:00:37', '2012-07-11 09:01:00', '2012-07-11 09:01:00', '2012-07-11 09:01:00', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/27''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/27\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/27''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/27\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/27\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/27\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 293, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(28, 1, 10, 'Test Moodle', '2012-07-11 09:01:24', '2012-07-11 09:01:24', '2012-07-11 09:01:45', '2012-07-11 09:01:45', '2012-07-11 09:01:45', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/28''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/28\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/28''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/28\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/28\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/28\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.004 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.004 sec wall, 0 syscalls)"}}', 2, 0, 294, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(29, 1, 9, 'Test Moodle', '2012-07-11 09:01:25', '2012-07-11 09:01:25', '2012-07-11 09:01:48', '2012-07-11 09:01:48', '2012-07-11 09:01:48', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/29''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/29\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/29''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/29''\\n.\\/\\/var\\/lz\\/temp\\/29\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/29\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/29''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 295, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(30, 1, 10, 'Test Moodle', '2012-07-11 09:01:25', '2012-07-11 09:01:25', '2012-07-11 09:02:23', '2012-07-11 09:02:23', '2012-07-11 09:02:23', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/30''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/30\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/30''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/30\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/30\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/30\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 296, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(31, 1, 9, 'Test Moodle', '2012-07-11 09:03:40', '2012-07-11 09:03:40', '2012-07-11 09:04:39', '2012-07-11 09:04:39', '2012-07-11 09:04:39', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/31''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/31\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/31''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/31''\\n.\\/\\/var\\/lz\\/temp\\/31\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/31\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/31''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 298, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(32, 1, 10, 'Test Moodle', '2012-07-11 09:03:40', '2012-07-11 09:03:40', '2012-07-11 09:04:50', '2012-07-11 09:04:50', '2012-07-11 09:04:50', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/32''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/32\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/32''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/32\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/32\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/32\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 299, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(33, 1, 9, 'Test Moodle', '2012-07-11 09:07:05', '2012-07-11 09:07:05', '2012-07-11 09:07:15', '2012-07-11 09:07:15', '2012-07-11 09:07:15', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/33''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/33\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/33''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/33''\\n.\\/\\/var\\/lz\\/temp\\/33\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/33\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/33''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 302, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(34, 1, 10, 'Test Moodle', '2012-07-11 09:07:05', '2012-07-11 09:07:05', '2012-07-11 09:07:19', '2012-07-11 09:07:19', '2012-07-11 09:07:19', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/34''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/34\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/34''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/34\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/34\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/34\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 303, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(35, 1, 9, 'Test Moodle', '2012-07-11 09:20:02', '2012-07-11 09:20:02', '2012-07-11 09:20:31', '2012-07-11 09:20:31', '2012-07-11 09:20:31', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/35''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/35\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/35''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/35''\\n.\\/\\/var\\/lz\\/temp\\/35\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/35\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/35''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 322, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(36, 1, 10, 'Test Moodle', '2012-07-11 09:20:02', '2012-07-11 09:20:02', '2012-07-11 09:20:34', '2012-07-11 09:20:34', '2012-07-11 09:20:34', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/36''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/36\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/36''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/36\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/36\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/36\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 323, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(37, 1, 10, 'Test Moodle', '2012-07-11 09:35:16', '2012-07-11 09:35:16', '2012-07-11 09:37:16', '2012-07-11 09:37:16', '2012-07-11 09:37:16', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/37''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/37\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/37''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/37\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/37\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/37\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 327, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(38, 1, 10, 'Test Moodle', '2012-07-11 09:36:28', '2012-07-11 09:36:28', '2012-07-11 09:37:20', '2012-07-11 09:37:20', '2012-07-11 09:37:20', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/38''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/38\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/38''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/38\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/38\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/38\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 328, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(39, 1, 9, 'Test Moodle', '2012-07-11 09:36:28', '2012-07-11 09:36:28', '2012-07-11 09:37:23', '2012-07-11 09:37:23', '2012-07-11 09:37:23', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/39''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/39\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/39''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/39''\\n.\\/\\/var\\/lz\\/temp\\/39\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/39\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/39''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 329, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(40, 1, 10, 'Test Moodle', '2012-07-11 09:36:28', '2012-07-11 09:36:28', '2012-07-11 09:37:27', '2012-07-11 09:37:27', '2012-07-11 09:37:27', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/40''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/40\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/40''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/40\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/40\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/40\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 330, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(41, 1, 10, 'Test Moodle', '2012-07-11 10:13:27', '2012-07-11 10:13:27', '2012-07-11 10:13:45', '2012-07-11 10:13:45', '2012-07-11 10:13:45', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/41''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/41\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/41''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/41\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.056 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/41\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/41\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 338, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(42, 1, 10, 'Test Moodle', '2012-07-11 10:13:35', '2012-07-11 10:13:35', '2012-07-11 10:13:49', '2012-07-11 10:13:49', '2012-07-11 10:13:49', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/42''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/42\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/42''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/42\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.003 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/42\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.007 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/42\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 339, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(43, 1, 9, 'Test Moodle', '2012-07-11 10:13:35', '2012-07-11 10:13:35', '2012-07-11 10:13:51', '2012-07-11 10:13:51', '2012-07-11 10:13:51', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/43''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/43\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/43''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/43''\\n.\\/\\/var\\/lz\\/temp\\/43\\/Main\\nmake: .\\/\\/var\\/lz\\/temp\\/43\\/Main: Command not found\\nmake: *** [grade] Error 127\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/43''\\n","return":1,"sandboxoutput":"Exited with error status 2"}}', 2, 0, 340, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000);
INSERT INTO `grade_requests` (`id`, `client_id`, `evaluationset_id`, `submitter_id`, `submitted_time`, `receive_time`, `dispatch_time`, `graded_time`, `report_time`, `source_file`, `action`, `report`, `status`, `mode`, `request_id`, `file`) VALUES
(44, 1, 10, 'Test Moodle', '2012-07-11 10:13:35', '2012-07-11 10:13:35', '2012-07-11 10:13:54', '2012-07-11 10:13:54', '2012-07-11 10:13:54', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/44''\\ngcc -c lib.c\\ng++ -o \\/var\\/lz\\/temp\\/44\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/44''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/44\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/44\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/44\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.006 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.006 sec wall, 0 syscalls)"}}', 2, 0, 341, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(45, 1, 10, 'Test Moodle', '2012-07-12 05:49:47', '2012-07-12 05:49:47', '2012-07-12 06:24:32', '2012-07-12 06:24:32', '2012-07-12 06:24:32', 'batch.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/45''\\nmake: \\/var\\/lz\\/temp\\/45\\/Makefile: No such file or directory\\nmake: *** No rule to make target `\\/var\\/lz\\/temp\\/45\\/Makefile''.  Stop.\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/45''","return":2},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/45\\/tc\\/1.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/45\\/tc\\/2.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/45\\/tc\\/3.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"},"score":0,"sandboxoutput":"Invalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"}}', 2, 0, 350, 0x504b03040a0000000000007aeb4057393d03020000000200000007001c00332e73636f726555540900033f36fd4f3f36fd4f75780b000104e803000004e8030000350a504b03040a0000000000f079eb40d55b0b31020000000200000005001c00332e6f757455540900032436fd4f3536fd4f75780b000104e803000004e8030000370a504b03040a0000000000f279eb406178e169040000000400000004001c00332e696e55540900032836fd4f3536fd4f75780b000104e803000004e8030000310a320a504b03040a0000000000fc79eb4044c34a9a030000000300000007001c00322e73636f726555540900033c36fd4f3c36fd4f75780b000104e803000004e803000032350a504b03040a00000000002c74eb401608261a020000000200000005001c00322e6f75745554090003432cfd4f4f2cfd4f75780b000104e803000004e8030000340a504b03040a00000000002b74eb408fd7547b040000000400000004001c00322e696e5554090003412cfd4f4f2cfd4f75780b000104e803000004e8030000320a320a504b03040a0000000000d979eb40eaf5f6e1030000000300000007001c00312e73636f72655554090003f935fd4f0b36fd4f75780b000104e803000004e803000037300a504b03040a00000000002974eb40d19e6755020000000200000005001c00312e6f757455540900033e2cfd4f4f2cfd4f75780b000104e803000004e8030000330a504b03040a00000000002874eb406178e169040000000400000004001c00312e696e55540900033c2cfd4f4f2cfd4f75780b000104e803000004e8030000310a320a504b01021e030a0000000000007aeb4057393d030200000002000000070018000000000001000000b48100000000332e73636f726555540500033f36fd4f75780b000104e803000004e8030000504b01021e030a0000000000f079eb40d55b0b310200000002000000050018000000000001000000b48143000000332e6f757455540500032436fd4f75780b000104e803000004e8030000504b01021e030a0000000000f279eb406178e1690400000004000000040018000000000001000000b48184000000332e696e55540500032836fd4f75780b000104e803000004e8030000504b01021e030a0000000000fc79eb4044c34a9a0300000003000000070018000000000001000000b481c6000000322e73636f726555540500033c36fd4f75780b000104e803000004e8030000504b01021e030a00000000002c74eb401608261a0200000002000000050018000000000001000000b4810a010000322e6f75745554050003432cfd4f75780b000104e803000004e8030000504b01021e030a00000000002b74eb408fd7547b0400000004000000040018000000000001000000b4814b010000322e696e5554050003412cfd4f75780b000104e803000004e8030000504b01021e030a0000000000d979eb40eaf5f6e10300000003000000070018000000000001000000b4818d010000312e73636f72655554050003f935fd4f75780b000104e803000004e8030000504b01021e030a00000000002974eb40d19e67550200000002000000050018000000000001000000b481d1010000312e6f757455540500033e2cfd4f75780b000104e803000004e8030000504b01021e030a00000000002874eb406178e1690400000004000000040018000000000001000000b48112020000312e696e55540500033c2cfd4f75780b000104e803000004e8030000504b05060000000009000900a6020000540200000000),
(46, 1, 10, 'Test Moodle', '2012-07-12 05:50:52', '2012-07-12 05:50:52', '2012-07-12 06:25:13', '2012-07-12 06:25:13', '2012-07-12 06:25:13', 'batch.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/46''\\nmake: \\/var\\/lz\\/temp\\/46\\/Makefile: No such file or directory\\nmake: *** No rule to make target `\\/var\\/lz\\/temp\\/46\\/Makefile''.  Stop.\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/46''","return":2},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/46\\/tc\\/1.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/46\\/tc\\/2.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/46\\/tc\\/3.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"},"score":0,"sandboxoutput":"Invalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"}}', 2, 0, 351, 0x504b03040a0000000000007aeb4057393d03020000000200000007001c00332e73636f726555540900033f36fd4f3f36fd4f75780b000104e803000004e8030000350a504b03040a0000000000f079eb40d55b0b31020000000200000005001c00332e6f757455540900032436fd4f3536fd4f75780b000104e803000004e8030000370a504b03040a0000000000f279eb406178e169040000000400000004001c00332e696e55540900032836fd4f3536fd4f75780b000104e803000004e8030000310a320a504b03040a0000000000fc79eb4044c34a9a030000000300000007001c00322e73636f726555540900033c36fd4f3c36fd4f75780b000104e803000004e803000032350a504b03040a00000000002c74eb401608261a020000000200000005001c00322e6f75745554090003432cfd4f4f2cfd4f75780b000104e803000004e8030000340a504b03040a00000000002b74eb408fd7547b040000000400000004001c00322e696e5554090003412cfd4f4f2cfd4f75780b000104e803000004e8030000320a320a504b03040a0000000000d979eb40eaf5f6e1030000000300000007001c00312e73636f72655554090003f935fd4f0b36fd4f75780b000104e803000004e803000037300a504b03040a00000000002974eb40d19e6755020000000200000005001c00312e6f757455540900033e2cfd4f4f2cfd4f75780b000104e803000004e8030000330a504b03040a00000000002874eb406178e169040000000400000004001c00312e696e55540900033c2cfd4f4f2cfd4f75780b000104e803000004e8030000310a320a504b01021e030a0000000000007aeb4057393d030200000002000000070018000000000001000000b48100000000332e73636f726555540500033f36fd4f75780b000104e803000004e8030000504b01021e030a0000000000f079eb40d55b0b310200000002000000050018000000000001000000b48143000000332e6f757455540500032436fd4f75780b000104e803000004e8030000504b01021e030a0000000000f279eb406178e1690400000004000000040018000000000001000000b48184000000332e696e55540500032836fd4f75780b000104e803000004e8030000504b01021e030a0000000000fc79eb4044c34a9a0300000003000000070018000000000001000000b481c6000000322e73636f726555540500033c36fd4f75780b000104e803000004e8030000504b01021e030a00000000002c74eb401608261a0200000002000000050018000000000001000000b4810a010000322e6f75745554050003432cfd4f75780b000104e803000004e8030000504b01021e030a00000000002b74eb408fd7547b0400000004000000040018000000000001000000b4814b010000322e696e5554050003412cfd4f75780b000104e803000004e8030000504b01021e030a0000000000d979eb40eaf5f6e10300000003000000070018000000000001000000b4818d010000312e73636f72655554050003f935fd4f75780b000104e803000004e8030000504b01021e030a00000000002974eb40d19e67550200000002000000050018000000000001000000b481d1010000312e6f757455540500033e2cfd4f75780b000104e803000004e8030000504b01021e030a00000000002874eb406178e1690400000004000000040018000000000001000000b48112020000312e696e55540500033c2cfd4f75780b000104e803000004e8030000504b05060000000009000900a6020000540200000000),
(47, 1, 10, 'Test Moodle', '2012-07-12 06:13:53', '2012-07-12 06:13:53', '2012-07-13 09:29:00', '2012-07-13 09:29:00', '2012-07-13 09:29:00', 'batch.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/47''\\nmake: \\/var\\/lz\\/temp\\/47\\/Makefile: No such file or directory\\nmake: *** No rule to make target `\\/var\\/lz\\/temp\\/47\\/Makefile''.  Stop.\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/47''","return":2},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/47\\/tc\\/1.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/47\\/tc\\/2.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/47\\/tc\\/3.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"},"score":0,"sandboxoutput":"Invalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"}}', 2, 0, 352, 0x504b03040a0000000000007aeb4057393d03020000000200000007001c00332e73636f726555540900033f36fd4f3f36fd4f75780b000104e803000004e8030000350a504b03040a0000000000f079eb40d55b0b31020000000200000005001c00332e6f757455540900032436fd4f3536fd4f75780b000104e803000004e8030000370a504b03040a0000000000f279eb406178e169040000000400000004001c00332e696e55540900032836fd4f3536fd4f75780b000104e803000004e8030000310a320a504b03040a0000000000fc79eb4044c34a9a030000000300000007001c00322e73636f726555540900033c36fd4f3c36fd4f75780b000104e803000004e803000032350a504b03040a00000000002c74eb401608261a020000000200000005001c00322e6f75745554090003432cfd4f4f2cfd4f75780b000104e803000004e8030000340a504b03040a00000000002b74eb408fd7547b040000000400000004001c00322e696e5554090003412cfd4f4f2cfd4f75780b000104e803000004e8030000320a320a504b03040a0000000000d979eb40eaf5f6e1030000000300000007001c00312e73636f72655554090003f935fd4f0b36fd4f75780b000104e803000004e803000037300a504b03040a00000000002974eb40d19e6755020000000200000005001c00312e6f757455540900033e2cfd4f4f2cfd4f75780b000104e803000004e8030000330a504b03040a00000000002874eb406178e169040000000400000004001c00312e696e55540900033c2cfd4f4f2cfd4f75780b000104e803000004e8030000310a320a504b01021e030a0000000000007aeb4057393d030200000002000000070018000000000001000000b48100000000332e73636f726555540500033f36fd4f75780b000104e803000004e8030000504b01021e030a0000000000f079eb40d55b0b310200000002000000050018000000000001000000b48143000000332e6f757455540500032436fd4f75780b000104e803000004e8030000504b01021e030a0000000000f279eb406178e1690400000004000000040018000000000001000000b48184000000332e696e55540500032836fd4f75780b000104e803000004e8030000504b01021e030a0000000000fc79eb4044c34a9a0300000003000000070018000000000001000000b481c6000000322e73636f726555540500033c36fd4f75780b000104e803000004e8030000504b01021e030a00000000002c74eb401608261a0200000002000000050018000000000001000000b4810a010000322e6f75745554050003432cfd4f75780b000104e803000004e8030000504b01021e030a00000000002b74eb408fd7547b0400000004000000040018000000000001000000b4814b010000322e696e5554050003412cfd4f75780b000104e803000004e8030000504b01021e030a0000000000d979eb40eaf5f6e10300000003000000070018000000000001000000b4818d010000312e73636f72655554050003f935fd4f75780b000104e803000004e8030000504b01021e030a00000000002974eb40d19e67550200000002000000050018000000000001000000b481d1010000312e6f757455540500033e2cfd4f75780b000104e803000004e8030000504b01021e030a00000000002874eb406178e1690400000004000000040018000000000001000000b48112020000312e696e55540500033c2cfd4f75780b000104e803000004e8030000504b05060000000009000900a6020000540200000000),
(48, 1, 9, 'Test Moodle', '2012-07-12 06:14:17', '2012-07-12 06:14:17', '2012-07-12 07:30:21', '2012-07-12 07:30:21', '2012-07-12 07:30:21', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/48''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/48\\/Main tester.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/48''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/48''\\n\\/var\\/lz\\/temp\\/48\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/48''\\n","return":0,"sandboxoutput":"OK (0.007 sec real, 0.011 sec wall, 0 syscalls)"}}', 2, 0, 353, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(49, 1, 10, 'Test Moodle', '2012-07-12 06:27:54', '2012-07-12 06:27:54', '2012-07-12 07:30:23', '2012-07-12 07:30:23', '2012-07-12 07:30:23', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/49''\\ng++ -o \\/var\\/lz\\/temp\\/49\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/49''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/49\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/49\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/49\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 365, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(50, 1, 9, 'Test Moodle', '2012-07-12 06:28:06', '2012-07-12 06:28:06', '2012-07-12 07:30:27', '2012-07-12 07:30:27', '2012-07-12 07:30:27', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/50''\\ng++ -c Point\\/Point.cpp\\ng++ -o \\/var\\/lz\\/temp\\/50\\/Main tester.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/50''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/50''\\n\\/var\\/lz\\/temp\\/50\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/50''\\n","return":0,"sandboxoutput":"OK (0.006 sec real, 0.008 sec wall, 0 syscalls)"}}', 2, 0, 366, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(51, 1, 9, 'Test Moodle', '2012-07-12 06:32:46', '2012-07-12 06:32:46', '2012-07-13 09:34:32', '2012-07-13 09:34:32', '2012-07-13 09:34:32', 'archive.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/51''\\ng++ -o \\/var\\/lz\\/temp\\/51\\/Main tester.cpp Point.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/51''","return":0},"grade_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/51''\\n\\/var\\/lz\\/temp\\/51\\/Main\\nDo something\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/51''\\n","return":0,"sandboxoutput":"OK (0.005 sec real, 0.007 sec wall, 0 syscalls)"}}', 2, 0, 378, 0x504b030414000000080003a48a406b18a9436b0000007a0000000a001c007465737465722e63707055540900035536844f5636844f75780b000104e803000004e8030000358c310ac33010046bef2b0ebbb11bc7bd42de10c80b8475c407d64948a72ae4ef1186345bcc303b89ee670b4ce33389daeddaf51831fdc55d52b5c23e3e805645dfa43e72cd7e67aa161cd00b8a5e745ee883e17aa0ec30e435a4578a6c47afe6a593c2d68ad2e6f0c50f504b0304140000000800d0a48a40434ab33a640000009e00000008001c004d616b6566696c655554090003d737844fd737844f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce30a710d0e710db22d492d2e492dd24b2e28e0e24acecf2dc8cc49d50589592904e467e695e8e57371a66b6b2be8e62ba868f883b56b025910cd9a70355c500654973e840b3214a23b19439c2bbd283125d58a8b136e2c1700504b030414000000080065a38a407edaee2860000000750000000d001c00506f696e742f506f696e742e6855540900032e35844f6635844f75780b000104e803000004e803000053ce4ccb4b494d5308f0f7f40b89f7e052067232f352e17caee49cc4e2628580fcccbc12856aae82d2a49ccc642b2e4eb08086a63517675966514969628e421d92507e668a424a7e707e6e6a4946665e3a48b0d69a8b8b4b39352f25338d0b00504b030414000000080089a38a4029f0c2c2630000008f0000000f001c00506f696e742f506f696e742e63707055540900037135844f7235844f75780b000104e803000004e803000053cecc4bce294d4955500ac8cfcc2bd1cb50e2528609d964e6179714a526e6da71718165adacc09486a64235572d5cac0e45b02c3f3345012a93921f9c9f9b5a929199970e96e62c2e49b1b24ace2f2d51b0b1515072c9572886c9c7e4295903b50300504b03040a000000000089a38a4000000000000000000000000006001c00506f696e742f55540900037135844f7c35844f75780b000104e803000004e8030000504b01021e0314000000080003a48a406b18a9436b0000007a0000000a0018000000000001000000b481000000007465737465722e63707055540500035536844f75780b000104e803000004e8030000504b01021e03140000000800d0a48a40434ab33a640000009e000000080018000000000001000000b481af0000004d616b6566696c655554050003d737844f75780b000104e803000004e8030000504b01021e0314000000080065a38a407edaee2860000000750000000d0018000000000001000000b48155010000506f696e742f506f696e742e6855540500032e35844f75780b000104e803000004e8030000504b01021e0314000000080089a38a4029f0c2c2630000008f0000000f0018000000000001000000b481fc010000506f696e742f506f696e742e63707055540500037135844f75780b000104e803000004e8030000504b01021e030a000000000089a38a40000000000000000000000000060018000000000000001000fd41a8020000506f696e742f55540500037135844f75780b000104e803000004e8030000504b0506000000000500050092010000e80200000000),
(52, 1, 10, 'Test Moodle', '2012-07-12 06:59:57', '2012-07-12 06:59:57', '2012-07-12 07:30:33', '2012-07-12 07:30:33', '2012-07-12 07:30:33', 'batch.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/52''\\nmake: \\/var\\/lz\\/temp\\/52\\/Makefile: No such file or directory\\nmake: *** No rule to make target `\\/var\\/lz\\/temp\\/52\\/Makefile''.  Stop.\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/52''","return":2},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/52\\/tc\\/1.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/52\\/tc\\/2.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/52\\/tc\\/3.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"},"score":0,"sandboxoutput":"Invalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"}}', 2, 0, 466, 0x504b03040a0000000000007aeb4057393d03020000000200000007001c00332e73636f726555540900033f36fd4f3f36fd4f75780b000104e803000004e8030000350a504b03040a0000000000f079eb40d55b0b31020000000200000005001c00332e6f757455540900032436fd4f3536fd4f75780b000104e803000004e8030000370a504b03040a0000000000f279eb406178e169040000000400000004001c00332e696e55540900032836fd4f3536fd4f75780b000104e803000004e8030000310a320a504b03040a0000000000fc79eb4044c34a9a030000000300000007001c00322e73636f726555540900033c36fd4f3c36fd4f75780b000104e803000004e803000032350a504b03040a00000000002c74eb401608261a020000000200000005001c00322e6f75745554090003432cfd4f4f2cfd4f75780b000104e803000004e8030000340a504b03040a00000000002b74eb408fd7547b040000000400000004001c00322e696e5554090003412cfd4f4f2cfd4f75780b000104e803000004e8030000320a320a504b03040a0000000000d979eb40eaf5f6e1030000000300000007001c00312e73636f72655554090003f935fd4f0b36fd4f75780b000104e803000004e803000037300a504b03040a00000000002974eb40d19e6755020000000200000005001c00312e6f757455540900033e2cfd4f4f2cfd4f75780b000104e803000004e8030000330a504b03040a00000000002874eb406178e169040000000400000004001c00312e696e55540900033c2cfd4f4f2cfd4f75780b000104e803000004e8030000310a320a504b01021e030a0000000000007aeb4057393d030200000002000000070018000000000001000000b48100000000332e73636f726555540500033f36fd4f75780b000104e803000004e8030000504b01021e030a0000000000f079eb40d55b0b310200000002000000050018000000000001000000b48143000000332e6f757455540500032436fd4f75780b000104e803000004e8030000504b01021e030a0000000000f279eb406178e1690400000004000000040018000000000001000000b48184000000332e696e55540500032836fd4f75780b000104e803000004e8030000504b01021e030a0000000000fc79eb4044c34a9a0300000003000000070018000000000001000000b481c6000000322e73636f726555540500033c36fd4f75780b000104e803000004e8030000504b01021e030a00000000002c74eb401608261a0200000002000000050018000000000001000000b4810a010000322e6f75745554050003432cfd4f75780b000104e803000004e8030000504b01021e030a00000000002b74eb408fd7547b0400000004000000040018000000000001000000b4814b010000322e696e5554050003412cfd4f75780b000104e803000004e8030000504b01021e030a0000000000d979eb40eaf5f6e10300000003000000070018000000000001000000b4818d010000312e73636f72655554050003f935fd4f75780b000104e803000004e8030000504b01021e030a00000000002974eb40d19e67550200000002000000050018000000000001000000b481d1010000312e6f757455540500033e2cfd4f75780b000104e803000004e8030000504b01021e030a00000000002874eb406178e1690400000004000000040018000000000001000000b48112020000312e696e55540500033c2cfd4f75780b000104e803000004e8030000504b05060000000009000900a6020000540200000000),
(53, 1, 10, 'Test Moodle', '2012-07-12 07:00:21', '2012-07-12 07:00:21', '2012-07-12 07:30:38', '2012-07-12 07:30:38', '2012-07-12 07:30:38', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/53''\\ng++ -o \\/var\\/lz\\/temp\\/53\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/53''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/53\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/53\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/53\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 467, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000);
INSERT INTO `grade_requests` (`id`, `client_id`, `evaluationset_id`, `submitter_id`, `submitted_time`, `receive_time`, `dispatch_time`, `graded_time`, `report_time`, `source_file`, `action`, `report`, `status`, `mode`, `request_id`, `file`) VALUES
(54, 1, 10, 'Test Moodle', '2012-07-12 07:00:25', '2012-07-12 07:00:25', '2012-07-12 07:30:41', '2012-07-12 07:30:41', '2012-07-12 07:30:41', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/54''\\ng++ -o \\/var\\/lz\\/temp\\/54\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/54''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/54\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/54\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.004 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/54\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 2, 0, 468, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(55, 1, 10, 'Test Moodle', '2012-07-12 07:13:00', '2012-07-12 07:13:00', '2012-07-12 07:30:44', '2012-07-12 07:30:44', '2012-07-12 07:30:44', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/55''\\ng++ -o \\/var\\/lz\\/temp\\/55\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/55''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/55\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/55\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.027 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/55\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.020 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.020 sec wall, 0 syscalls)"}}', 0, 0, 499, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(56, 1, 10, 'Test Moodle', '2012-07-12 07:23:24', '2012-07-12 07:23:24', '2012-07-12 07:30:47', '2012-07-12 07:30:47', '2012-07-12 07:30:47', 'answer.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/56''\\ng++ -o \\/var\\/lz\\/temp\\/56\\/Main main.c lib.o\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/56''","return":0},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/56\\/tc\\/1.in":"Correct (70)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/56\\/tc\\/2.in":"Correct (25)\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)","\\/var\\/lz\\/temp\\/56\\/tc\\/3.in":"Wrong answer\\nOK (0.001 sec real, 0.002 sec wall, 0 syscalls)"},"score":95,"sandboxoutput":"OK (0.001 sec real, 0.002 sec wall, 0 syscalls)"}}', 0, 0, 522, 0x504b0304140000000800e175eb40343d940a5c0000007e00000008001c004d616b6566696c655554090003762ffd4f792ffd4f75780b000104e803000004e8030000f30f0d09080db1cd4dcccce3f275f4f403b3f492b9b892f3730b327352754b528b4bac14723293f4f2b938d3b5b51574f3155434fcc1da34812c90264da83c179882a84e06aa4e4e56d04d86f2b8d28b125352adb838f5f4e1dab900504b03041400000008002d75eb40948ac8665e0000007c00000006001c006d61696e2e635554090003252efd4f9e2efd4f75780b000104e803000004e803000053cecc4bce294d4955b0292e49c9ccd7cbb0e352860929e56426e965287171e52666e66994e567a6682a54737166e6952824ea2824597371162727e6a56928a9a62829e828a8256a62082581840a8a805ac062317920d1446d90702d1700504b03041400000008001075eb40d9d5f765310000003b00000005001c006c69622e685554090003ef2dfd4f932efd4f75780b000104e803000004e803000053ce4ccb4b494d53f0f1748af7e052063233f352a13caeccbc1285e2d25c0d109da803229334adb9b89453f35232d3b800504b0304140000000800cd75eb407570b1af380000003b00000005001c006c69622e635554090003512ffd4f522ffd4f75780b000104e803000004e803000053cecc4bce294d495550cac94cd2cb50e2e2cacc2b51282ecdd500d1893a0a202a4953a19a8bb328b5a4b4284f21513bc99a8b93ab960b00504b01021e03140000000800e175eb40343d940a5c0000007e000000080018000000000001000000b481000000004d616b6566696c655554050003762ffd4f75780b000104e803000004e8030000504b01021e031400000008002d75eb40948ac8665e0000007c000000060018000000000001000000b4819e0000006d61696e2e635554050003252efd4f75780b000104e803000004e8030000504b01021e031400000008001075eb40d9d5f765310000003b000000050018000000000001000000b4813c0100006c69622e685554050003ef2dfd4f75780b000104e803000004e8030000504b01021e03140000000800cd75eb407570b1af380000003b000000050018000000000001000000b481ac0100006c69622e635554050003512ffd4f75780b000104e803000004e8030000504b0506000000000400040030010000230200000000),
(57, 100, 10, 'Karol Danutama', '2012-07-12 08:33:04', '2012-07-13 09:37:25', '2012-07-13 09:37:28', '2012-07-13 09:37:28', '2012-07-13 09:37:28', 'grade.zip', 1, '{"compile_result":{"output":"make: Entering directory `\\/var\\/lz\\/temp\\/57''\\nmake: \\/var\\/lz\\/temp\\/57\\/Makefile: No such file or directory\\nmake: *** No rule to make target `\\/var\\/lz\\/temp\\/57\\/Makefile''.  Stop.\\nmake: Leaving directory `\\/var\\/lz\\/temp\\/57''","return":2},"grade_result":{"detailedresult":{"\\/var\\/lz\\/temp\\/57\\/tc\\/1.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/57\\/tc\\/2.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)","\\/var\\/lz\\/temp\\/57\\/tc\\/3.in":"Execution failure\\nInvalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"},"score":0,"sandboxoutput":"Invalid arguments!\\nUsage: box [<options>] -- <command> <arguments>\\n\\nOptions:\\n-a <level>\\tSet file access level (0=none, 1=cwd, 2=\\/etc,\\/lib,..., 3=whole fs, 9=no checks; needs -f)\\n-c <dir>\\tChange directory to <dir> first\\n-e\\t\\tInherit full environment of the parent process\\n-E <var>\\tInherit the environment variable <var> from the parent process\\n-E <var>=<val>\\tSet the environment variable <var> to <val>; unset it if <var> is empty\\n-f\\t\\tFilter system calls (-ff=very restricted)\\n-i <file>\\tRedirect stdin from <file>\\n-m <size>\\tLimit address space to <size> KB\\n-o <file>\\tRedirect stdout to <file>\\n-p <path>\\tPermit access to the specified path (or subtree if it ends with a `\\/'')\\n-p <path>=<act>\\tDefine action for the specified path (<act>=yes\\/no)\\n-r <file>\\tRedirect stderr to <file>\\n-s <sys>\\tPermit the specified syscall (be careful)\\n-s <sys>=<act>\\tDefine action for the specified syscall (<act>=yes\\/no\\/file)\\n-t <time>\\tSet run time limit (seconds, fractions allowed)\\n-T\\t\\tAllow syscalls for measuring run time\\n-v\\t\\tBe verbose (use multiple times for even more verbosity)\\n-w <time>\\tSet wall clock time limit (seconds, fractions allowed)"}}', 2, 0, 534, 0x504b0304140000000800227cec40f819e45a050000000600000008001c006d61696e2e6370705554090003c08bfe4fc08bfe4f75780b0001043000000004300000004b4c040100504b03040a0000000000227cec40b993acee050000000500000008001c00746573742e6370705554090003c08bfe4fc08bfe4f75780b0001043000000004300000006161616161504b03040a0000000000227cec40b993acee050000000500000006001c00746573742e685554090003c08bfe4fc08bfe4f75780b0001043000000004300000006161616161504b01021e03140000000800227cec40f819e45a0500000006000000080018000000000001000000a481000000006d61696e2e6370705554050003c08bfe4f75780b000104300000000430000000504b01021e030a0000000000227cec40b993acee0500000005000000080018000000000001000000a48147000000746573742e6370705554050003c08bfe4f75780b000104300000000430000000504b01021e030a0000000000227cec40b993acee0500000005000000060018000000000001000000a4818e000000746573742e685554050003c08bfe4f75780b000104300000000430000000504b05060000000003000300e8000000d30000000000);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'administrator', 'Administrator'),
(2, 'supervisor', 'Supervisor'),
(3, 'learner', 'Learner');

-- --------------------------------------------------------

--
-- Table structure for table `groups_users`
--

CREATE TABLE IF NOT EXISTS `groups_users` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups_users`
--

INSERT INTO `groups_users` (`group_id`, `user_id`) VALUES
(1, 1),
(1, 8),
(2, 8),
(3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(128) DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `logtime` int(11) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pastebin`
--

CREATE TABLE IF NOT EXISTS `pastebin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `type` varchar(64) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pastebin`
--

INSERT INTO `pastebin` (`id`, `owner_id`, `type`, `status`, `created_date`, `content`) VALUES
(3, 8, 'cpp', 1, '2011-11-17 20:22:22', '/*\r\nTemplate checker interaktif\r\n*/\r\n\r\n#include <iostream>\r\n#include <fstream>\r\n#include <cstdio>\r\n#include <sys/wait.h>\r\n#include <cassert>\r\n#include <cstring>\r\n#include <unistd.h>\r\n#include <cstdlib>\r\n#include <sys/time.h>\r\n#include <sys/time.h>\r\n#include <sys/resource.h>\r\n#include <signal.h>\r\n#include <sys/ptrace.h>\r\n\r\nusing namespace std;\r\n\r\n/**\r\n * Process control variables\r\n * **/\r\n\r\n/**\r\n * Problem specific variables\r\n * */\r\nint guessLimit;\r\nstring angka;\r\n\r\nvoid readInput(char* pInputFile) {\r\n	/**\r\n	 * Modify this function to read input file given in argv\r\n	 * */\r\n}\r\n\r\n\r\nvoid interact() {\r\n/*\r\nDo interaction here, using cin and cout\r\n*/\r\n\r\n\r\n/*\r\nFinish interaction and grading here.\r\nOutput 2 lines to stderr (cerr)\r\nLine 1 : "[OK]" or "[FAILED]"\r\nLine 2 : Reason or any feedback\r\n*/\r\n}\r\n\r\nint main(int argc , char** argv) {\r\n	assert(argc == 2);\r\n	\r\n	readInput(argv[1]);\r\n	interact();\r\n	\r\n	return 0;\r\n}\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `privatemessages`
--

CREATE TABLE IF NOT EXISTS `privatemessages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `send_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Storing private messaging' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `privatemessages_recipients`
--

CREATE TABLE IF NOT EXISTS `privatemessages_recipients` (
  `privatemessage_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `unread` tinyint(1) NOT NULL,
  KEY `recipient_id` (`recipient_id`),
  KEY `private_message_id` (`privatemessage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Storing private messaging recipients';

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE IF NOT EXISTS `problems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `problem_type_id` int(10) unsigned NOT NULL,
  `description` text,
  `token` varchar(32) NOT NULL,
  `visibility` int(11) NOT NULL,
  `available_languages` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `author_id` (`author_id`),
  KEY `problem_type_id` (`problem_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Storing problems' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `problemsets`
--

CREATE TABLE IF NOT EXISTS `problemsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `problemsets`
--

INSERT INTO `problemsets` (`id`, `parent_id`, `status`, `created_date`, `modified_date`, `name`, `description`) VALUES
(1, NULL, 0, '2010-07-15 15:49:24', '2010-12-08 14:25:49', 'Semua Soal', 'Bundel Soal Perkenalan'),
(2, NULL, 1, '2010-07-18 21:05:33', '2010-12-07 13:48:16', 'Bundel Soal Perkenalan', 'Bundel Soal Perkenalan'),
(3, NULL, 0, '2010-07-18 21:05:49', '2010-07-18 21:05:49', 'Bundel Soal 2006', 'Bundel Soal 2006'),
(4, NULL, 0, '2010-07-18 21:06:36', '2010-07-18 21:06:36', 'Bundel Soal 2007', 'Bundel Soal 2007'),
(5, 13, 1, '2010-07-18 21:06:57', '2010-11-12 22:40:54', 'Bundel Soal 2008', 'Bundel Soal 2008'),
(6, 13, 1, '2010-07-18 21:07:25', '2010-09-13 06:58:15', 'Bundel Soal 2009', 'Bundel Soal 2009'),
(7, 13, 1, '2010-07-18 21:07:52', '2010-09-13 06:58:20', 'Bundel Soal 2010', 'Kumpulan soal yang digunakan pada tahun 2010'),
(8, NULL, 0, '2010-07-28 00:58:12', '2010-07-28 00:58:12', 'Pelatda DKI 2009', 'Soal - soal simulasi yang digunakan pada Pelatda DKI 2009'),
(9, 6, 1, '2010-07-30 08:10:07', '2010-07-30 08:10:37', 'OSN 2009', 'Olimpiade Sains Nasional 2009, DKI Jakarta'),
(10, 14, 1, '2010-08-02 23:16:26', '2010-09-13 07:01:34', 'OSN 2010 Sesi 2', 'Soal - soal Olimpiade Sains Nasional 2010, Sumatera Utara, Sesi kedua: Pemrograman sederhana.'),
(11, 14, 1, '2010-08-02 23:22:30', '2010-09-13 07:01:31', 'OSN 2010 Sesi 0', 'Soal - soal Olimpiade Sains Nasional 2010, Sumatera Utara, Sesi latihan.'),
(12, 14, 1, '2010-08-04 09:57:27', '2010-09-13 07:01:38', 'OSN 2010 Sesi 3', 'Soal - soal Olimpiade Sains Nasional 2010, Sumatera Utara, Sesi ketiga.'),
(13, NULL, 1, '2010-09-13 06:57:59', '2010-12-08 14:50:53', 'Arsip Kontes', 'Bundel soal berisi soal-soal yang disusun berdasarkan waktu/acara yang menggunakan soal-soal tersebut.'),
(14, 7, 1, '2010-09-13 07:01:24', '2010-09-13 07:02:03', 'OSN 2010', 'Soal-soal Olimpiade Sains Nasional 2010, Sumatera Utara.'),
(15, NULL, 0, '2010-09-13 23:11:20', '2010-09-13 23:11:20', 'OSN 2008', 'Olimpiade Sains Nasional 2008, Makassar Sulawesi Selatan'),
(16, 6, 1, '2010-09-13 23:13:08', '2010-09-14 14:07:59', 'BNPC HS 2009', 'Bina Nusantara Programming Contest for High School Students 2009'),
(17, 5, 1, '2010-09-13 23:13:20', '2010-09-14 07:02:03', 'BNPC HS 2008', 'Bina Nusantara Programming Contest for High School Students 2008'),
(18, 7, 1, '2010-09-28 14:57:44', '2010-09-28 16:58:37', 'ITBPC 2010', 'ITB Programming Contest 2010'),
(19, 18, 1, '2010-09-28 14:58:19', '2010-09-28 16:58:41', 'ITBPC 2010 Penyisihan tingkat SMA', 'Soal Penyisihan ITB Programming Contest 2010 tingkat SMA'),
(20, 18, 1, '2010-09-28 14:58:32', '2011-08-10 18:33:24', 'ITBPC 2010 Penyisihan tingkat Perguruan Tinggi', 'Soal Penyisihan ITB Programming Contest 2010 tingkat Perguruan Tinggi'),
(21, 18, 1, '2010-09-28 14:58:54', '2010-09-29 10:04:27', 'ITBPC 2010 Final tingkat SMA', 'Soal Final ITB Programming Contest 2010 tingkat SMA'),
(22, 18, 1, '2010-09-28 14:59:05', '2011-08-10 18:33:18', 'ITBPC 2010 Final tingkat Perguruan Tinggi', 'Soal Final ITB Programming Contest 2010 tingkat Perguruan Tinggi'),
(23, 5, 1, '2010-10-29 19:10:05', '2010-11-08 16:38:20', 'JOINTS 2008', 'Jogja Information Technology Session 2008'),
(24, 6, 1, '2010-10-29 19:10:26', '2010-11-01 11:39:17', 'JOINTS 2009', 'Jogja Information Technology Session 2009'),
(25, 7, 1, '2010-11-01 06:30:09', '2010-11-08 05:37:40', 'BNPC HS 2010', 'Bina Nusantara Programming Contest for High School Students 2010'),
(26, 25, 1, '2010-11-08 04:42:09', '2010-11-08 05:37:43', 'BNPC HS 2010 Penyisihan', 'Bina Nusantara Programming Contest for High School Students 2010\r\nPenyisihan'),
(27, 25, 1, '2010-11-08 04:42:22', '2010-11-08 11:34:11', 'BNPC HS 2010 Final', 'Bina Nusantara Programming Contest for High School Students 2010\r\nFinal'),
(28, 7, 1, '2010-11-10 08:42:29', '2010-11-10 09:26:14', 'INC 2010', 'ACM-ICPC Indonesia National Contest 2010'),
(29, 5, 1, '2010-11-12 22:37:02', '2010-11-12 22:41:00', 'Padmanaba Programming Competition 2008', 'Kompetisi pemrograman yang diselenggarakan SMA 3 Yogyakarta tahun 2008'),
(30, 6, 1, '2010-12-04 04:49:42', '2010-12-04 18:22:51', 'Schematics 2009', 'Schematics 2009'),
(31, 6, 1, '2010-12-16 18:55:34', '2010-12-17 07:14:22', 'INC 2009', 'ACM-ICPC Indonesia National Contest 2009'),
(32, 5, 1, '2010-12-16 18:56:10', '2010-12-22 14:05:28', 'ICPC 2008 Jakarta', 'ACM ICPC 2008 Regional Asia/Jakarta'),
(33, NULL, 0, '2010-12-16 18:56:23', '2010-12-16 18:56:23', 'ICPC 2009 Jakarta', 'ACM ICPC 2009 Regional Asia/Jakarta'),
(34, NULL, 0, '2010-12-16 18:56:45', '2010-12-16 18:56:45', 'ICPC 2010 Jakarta', 'ACM ICPC 2010 Regional Asia/Jakarta'),
(35, 7, 1, '2010-12-16 18:59:06', '2010-12-19 23:41:46', 'TOKI OpenContest December 2010', 'TOKI OpenContest December 2010'),
(36, 37, 1, '2011-02-01 08:29:43', '2011-02-01 08:31:40', 'TOKI OpenContest January 2011', 'TOKI OpenContest January 2011'),
(37, 13, 1, '2011-02-01 08:30:15', '2011-02-01 08:30:47', 'Bundel Soal 2011', 'Kumpulan soal yang digunakan pada tahun 2011'),
(38, 37, 0, '2011-02-26 14:57:14', '2011-02-26 15:07:42', 'Olimpiade Komputer Unisbank 2011', 'Olimpiade Komputer Unisbank 2011'),
(39, 37, 1, '2011-03-01 13:13:22', '2011-03-01 13:15:05', 'TOKI Open Contest Februari 2011', 'TOKI Open Contest Februari 2011'),
(40, 37, 1, '2011-03-01 13:15:40', '2011-03-01 13:27:43', 'Babak Final Programming Contest Arkavidia 2011', 'Babak Final Programming Contest Arkavidia 2011'),
(41, NULL, 0, '2011-03-01 20:45:56', '2011-03-01 20:47:43', 'TOKI OpenContest March 2011', 'TOKI OpenContest March 2011'),
(42, 37, 1, '2011-04-10 22:15:57', '2011-05-10 19:48:18', 'Toki "Special" Open Contest April', 'Maximum score = 409.5 arkidd. Congratulations! Jago2 ngalahin fushar.'),
(43, 37, 1, '2011-05-25 13:58:21', '2011-05-25 14:03:42', 'JOINTS 2011', 'Final PCS JOINTS 2011'),
(44, NULL, 1, '2011-06-15 23:20:26', '2011-06-15 23:21:33', 'Etcetera', 'Soal2 yang ga masuk klasifikasi mana2'),
(45, 44, 1, '2011-06-15 23:21:06', '2011-06-15 23:21:54', 'Irvan', 'Soal2 buatan irvan arsip'),
(46, NULL, 0, '2012-01-23 15:05:23', '2012-01-23 15:06:45', 'BNPC HS 2011', 'Bina Nusantara Programming Contest for High School Students 2011'),
(47, 46, 0, '2012-01-23 15:06:27', '2012-01-23 15:07:00', 'BNPC HS 2011 Final', 'Bina Nusantara Programming Contest for High School Students 2011 Final'),
(48, NULL, 0, '2012-01-27 21:41:38', '2012-01-27 21:41:38', 'Compfest 2011', 'Compfest 2011 oleh Universitas Indonesia'),
(49, 48, 0, '2012-01-27 21:42:01', '2012-01-27 21:43:40', 'Compfest 2011 - Final Perguruan Tinggi', 'Compfest 2011 - Final Perguruan Tinggi oleh Universitas Indonesia'),
(50, NULL, 0, '2012-01-27 21:42:32', '2012-01-27 21:42:35', 'ITBPC 2011', 'ITBPC 2011 oleh Institut Teknologi Bandung'),
(51, 48, 0, '2012-01-27 21:42:34', '2012-01-27 21:43:43', 'Compfest 2011 - Final SMA', 'Compfest 2011 - Final SMA oleh Universitas Indonesia'),
(52, 48, 0, '2012-01-27 21:42:56', '2012-01-27 21:43:47', 'Compfest 2011 - Penyisihan Perguruan Tinggi', 'Compfest 2011 - Penyisihan Perguruan Tinggi oleh Universitas Indonesia'),
(53, 48, 0, '2012-01-27 21:43:17', '2012-01-27 21:43:52', 'Compfest 2011 - Penyisihan SMA', 'Compfest 2011 - Penyisihan SMA oleh Universitas Indonesia'),
(54, 50, 0, '2012-01-27 21:43:33', '2012-01-27 21:45:11', 'ITBSPC', 'ITBSPC 2011 oleh Institut Teknologi Bandung'),
(55, 50, 0, '2012-01-27 21:44:30', '2012-01-27 21:45:16', 'ITBJPC', 'ITBJPC oleh Institut Teknologi Bandung'),
(56, 55, 0, '2012-01-27 22:02:18', '2012-01-27 22:04:14', 'ITBJPC - Penyisihan', 'ITBJPC - Penyisihan oleh Institut Teknologi Bandung'),
(57, 55, 0, '2012-01-27 22:02:56', '2012-01-27 22:04:21', 'ITBJPC - Final', 'ITBJPC - Final oleh Institut Teknologi Bandung'),
(58, 54, 0, '2012-01-27 22:03:35', '2012-01-27 22:04:50', 'ITBSPC - Penyisihan', 'ITBSPC - Penyisihan oleh Institut Teknologi Bandung'),
(59, 54, 0, '2012-01-27 22:03:54', '2012-01-27 22:04:45', 'ITBSPC - Final', 'ITBSPC - Final oleh Institut Teknologi Bandung'),
(60, NULL, 0, '2012-01-27 22:25:42', '2012-01-27 22:26:34', 'INC 2011', 'INC 2011 oleh Bina Nusantara University'),
(61, 60, 0, '2012-01-27 22:28:14', '2012-01-27 22:30:19', 'INC 2011 - Penyisihan', 'INC 2011 - Penyisihan oleh Bina Nusantara University'),
(62, 60, 0, '2012-01-27 22:28:33', '2012-01-27 22:31:05', 'INC 2011 - Final', 'INC 2011 - Final oleh Bina Nusantara University'),
(63, NULL, 0, '2012-01-27 22:28:52', '2012-01-27 22:28:52', 'BNPC HS 2011', 'BNPC HS 2011 oleh Bina Nusantara University'),
(64, 63, 0, '2012-01-27 22:29:09', '2012-01-27 22:29:54', 'BNPC HS 2011 - Penyisihan', 'BNPC HS 2011 - Penyisihan oleh Bina Nusantara University'),
(65, 63, 0, '2012-01-27 22:29:32', '2012-01-27 22:30:06', 'BNPC HS 2011 - Final', 'BNPC HS 2011 - Final oleh Bina Nusantara University');

-- --------------------------------------------------------

--
-- Table structure for table `problemsets_problems`
--

CREATE TABLE IF NOT EXISTS `problemsets_problems` (
  `problemset_id` int(11) NOT NULL,
  `problem_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`problemset_id`,`problem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `problemsets_problems`
--

INSERT INTO `problemsets_problems` (`problemset_id`, `problem_id`, `status`, `rank`) VALUES
(1, 1, 0, NULL),
(10, 21, 0, NULL),
(10, 22, 0, NULL),
(10, 23, 0, NULL),
(10, 24, 0, NULL),
(10, 25, 0, NULL),
(8, 10, 0, NULL),
(8, 11, 0, NULL),
(8, 8, 0, NULL),
(8, 9, 0, NULL),
(9, 2, 0, NULL),
(9, 3, 0, NULL),
(9, 4, 0, NULL),
(9, 5, 0, NULL),
(9, 6, 0, NULL),
(10, 26, 0, NULL),
(10, 27, 0, NULL),
(10, 28, 0, NULL),
(11, 16, 0, NULL),
(11, 17, 0, NULL),
(11, 18, 0, NULL),
(11, 19, 0, NULL),
(11, 20, 0, NULL),
(12, 15, 0, NULL),
(12, 30, 0, NULL),
(12, 31, 0, NULL),
(12, 32, 0, NULL),
(12, 33, 0, NULL),
(17, 58, 0, NULL),
(17, 59, 0, NULL),
(17, 60, 0, NULL),
(17, 61, 0, NULL),
(17, 62, 0, NULL),
(17, 63, 0, NULL),
(17, 64, 0, NULL),
(17, 65, 0, NULL),
(16, 66, 0, NULL),
(16, 67, 0, NULL),
(16, 68, 0, NULL),
(16, 69, 0, NULL),
(16, 70, 0, NULL),
(16, 71, 0, NULL),
(16, 72, 0, NULL),
(16, 73, 0, NULL),
(16, 74, 0, NULL),
(16, 75, 0, NULL),
(19, 98, 0, NULL),
(19, 99, 0, NULL),
(19, 96, 0, NULL),
(19, 97, 0, NULL),
(19, 100, 0, NULL),
(19, 101, 0, NULL),
(19, 102, 0, NULL),
(19, 103, 0, NULL),
(21, 104, 0, NULL),
(21, 105, 0, NULL),
(21, 106, 0, NULL),
(21, 107, 0, NULL),
(23, 192, 0, NULL),
(23, 193, 0, NULL),
(23, 194, 0, NULL),
(23, 195, 0, NULL),
(23, 196, 0, NULL),
(24, 197, 0, NULL),
(24, 198, 0, NULL),
(24, 199, 0, NULL),
(24, 200, 0, NULL),
(24, 201, 0, NULL),
(26, 216, 0, NULL),
(26, 217, 0, NULL),
(26, 218, 0, NULL),
(27, 219, 0, NULL),
(27, 220, 0, NULL),
(27, 221, 0, NULL),
(27, 222, 0, NULL),
(27, 223, 0, NULL),
(27, 224, 0, NULL),
(27, 225, 0, NULL),
(27, 226, 0, NULL),
(27, 227, 0, NULL),
(27, 228, 0, NULL),
(28, 233, 0, NULL),
(28, 234, 0, NULL),
(28, 235, 0, NULL),
(28, 236, 0, NULL),
(28, 237, 0, NULL),
(28, 238, 0, NULL),
(28, 239, 0, NULL),
(28, 240, 0, NULL),
(28, 241, 0, NULL),
(1, 244, 0, NULL),
(29, 249, 0, NULL),
(29, 248, 0, NULL),
(29, 247, 0, NULL),
(29, 246, 0, NULL),
(29, 245, 0, NULL),
(30, 331, 0, NULL),
(30, 332, 0, NULL),
(30, 333, 0, NULL),
(30, 334, 0, NULL),
(30, 335, 0, NULL),
(30, 336, 0, NULL),
(30, 337, 0, NULL),
(2, 1, 0, NULL),
(1, 2, 0, NULL),
(1, 3, 0, NULL),
(1, 4, 0, NULL),
(1, 5, 0, NULL),
(1, 6, 0, NULL),
(1, 8, 0, NULL),
(1, 9, 0, NULL),
(1, 10, 0, NULL),
(1, 11, 0, NULL),
(1, 18, 0, NULL),
(1, 19, 0, NULL),
(1, 20, 0, NULL),
(1, 21, 0, NULL),
(1, 22, 0, NULL),
(1, 23, 0, NULL),
(1, 24, 0, NULL),
(1, 25, 0, NULL),
(1, 26, 0, NULL),
(1, 27, 0, NULL),
(1, 28, 0, NULL),
(1, 30, 0, NULL),
(1, 31, 0, NULL),
(1, 32, 0, NULL),
(1, 33, 0, NULL),
(1, 58, 0, NULL),
(1, 59, 0, NULL),
(1, 60, 0, NULL),
(1, 61, 0, NULL),
(1, 62, 0, NULL),
(1, 63, 0, NULL),
(1, 64, 0, NULL),
(1, 65, 0, NULL),
(1, 66, 0, NULL),
(1, 67, 0, NULL),
(1, 68, 0, NULL),
(1, 69, 0, NULL),
(1, 70, 0, NULL),
(1, 71, 0, NULL),
(1, 72, 0, NULL),
(1, 73, 0, NULL),
(1, 74, 0, NULL),
(1, 75, 0, NULL),
(1, 96, 0, NULL),
(1, 97, 0, NULL),
(1, 98, 0, NULL),
(1, 99, 0, NULL),
(1, 102, 0, NULL),
(1, 103, 0, NULL),
(1, 104, 0, NULL),
(1, 105, 0, NULL),
(1, 106, 0, NULL),
(1, 107, 0, NULL),
(1, 161, 0, NULL),
(1, 192, 0, NULL),
(1, 193, 0, NULL),
(1, 194, 0, NULL),
(1, 199, 0, NULL),
(1, 200, 0, NULL),
(1, 201, 0, NULL),
(1, 216, 0, NULL),
(1, 217, 0, NULL),
(1, 218, 0, NULL),
(1, 219, 0, NULL),
(1, 220, 0, NULL),
(1, 221, 0, NULL),
(1, 222, 0, NULL),
(1, 223, 0, NULL),
(1, 224, 0, NULL),
(1, 225, 0, NULL),
(1, 226, 0, NULL),
(1, 227, 0, NULL),
(1, 228, 0, NULL),
(1, 233, 0, NULL),
(1, 234, 0, NULL),
(1, 235, 0, NULL),
(1, 236, 0, NULL),
(1, 237, 0, NULL),
(1, 238, 0, NULL),
(1, 239, 0, NULL),
(1, 240, 0, NULL),
(1, 241, 0, NULL),
(1, 245, 0, NULL),
(1, 246, 0, NULL),
(1, 247, 0, NULL),
(1, 248, 0, NULL),
(1, 249, 0, NULL),
(1, 331, 0, NULL),
(1, 332, 0, NULL),
(1, 333, 0, NULL),
(1, 334, 0, NULL),
(1, 335, 0, NULL),
(1, 336, 0, NULL),
(1, 337, 0, NULL),
(35, 338, 0, NULL),
(35, 105, 0, NULL),
(31, 341, 0, NULL),
(31, 342, 0, NULL),
(31, 343, 0, NULL),
(31, 344, 0, NULL),
(31, 345, 0, NULL),
(31, 346, 0, NULL),
(1, 341, 0, NULL),
(1, 342, 0, NULL),
(1, 343, 0, NULL),
(1, 344, 0, NULL),
(1, 345, 0, NULL),
(1, 346, 0, NULL),
(35, 357, 0, NULL),
(35, 339, 0, NULL),
(1, 338, 0, NULL),
(1, 339, 0, NULL),
(1, 357, 0, NULL),
(32, 347, 0, NULL),
(32, 348, 0, NULL),
(32, 349, 0, NULL),
(32, 350, 0, NULL),
(32, 351, 0, NULL),
(32, 352, 0, NULL),
(32, 353, 0, NULL),
(32, 354, 0, NULL),
(32, 355, 0, NULL),
(32, 356, 0, NULL),
(1, 347, 0, NULL),
(1, 348, 0, NULL),
(1, 349, 0, NULL),
(1, 350, 0, NULL),
(1, 351, 0, NULL),
(1, 352, 0, NULL),
(1, 353, 0, NULL),
(1, 354, 0, NULL),
(1, 355, 0, NULL),
(1, 356, 0, NULL),
(36, 445, 0, NULL),
(36, 446, 0, NULL),
(36, 447, 0, NULL),
(36, 448, 0, NULL),
(39, 477, 0, NULL),
(39, 476, 0, NULL),
(39, 475, 0, NULL),
(39, 474, 0, NULL),
(40, 473, 0, NULL),
(40, 472, 0, NULL),
(40, 469, 0, NULL),
(40, 468, 0, NULL),
(40, 467, 0, NULL),
(40, 457, 0, NULL),
(42, 502, 0, NULL),
(42, 503, 0, NULL),
(42, 512, 0, NULL),
(42, 505, 0, NULL),
(42, 506, 0, NULL),
(42, 507, 0, NULL),
(42, 508, 0, NULL),
(43, 575, 0, NULL),
(43, 576, 0, NULL),
(43, 577, 0, NULL),
(43, 578, 0, NULL),
(43, 579, 0, NULL),
(45, 582, 0, NULL),
(45, 583, 0, NULL),
(45, 584, 0, NULL),
(45, 585, 0, NULL),
(45, 586, 0, NULL),
(20, 54, 0, NULL),
(20, 52, 0, NULL),
(20, 51, 0, NULL),
(20, 50, 0, NULL),
(20, 49, 0, NULL),
(20, 48, 0, NULL),
(22, 87, 0, NULL),
(22, 86, 0, NULL),
(22, 85, 0, NULL),
(22, 84, 0, NULL),
(22, 83, 0, NULL),
(22, 82, 0, NULL),
(22, 81, 0, NULL),
(22, 79, 0, NULL),
(56, 958, 0, NULL),
(56, 971, 0, NULL),
(53, 957, 0, NULL),
(53, 960, 0, NULL),
(53, 969, 0, NULL),
(53, 970, 0, NULL),
(53, 972, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `problem_privileges`
--

CREATE TABLE IF NOT EXISTS `problem_privileges` (
  `problem_id` int(14) unsigned NOT NULL,
  `user_id` int(14) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `problem_privileges`
--

INSERT INTO `problem_privileges` (`problem_id`, `user_id`) VALUES
(8, 10),
(8, 22),
(857, 8),
(857, 10),
(857, 1214),
(856, 8),
(856, 10),
(856, 1214),
(859, 8),
(859, 10),
(859, 1214),
(859, 5),
(858, 8),
(858, 5),
(858, 10),
(858, 1214),
(858, 1291),
(859, 1291),
(857, 1291),
(857, 5),
(856, 3998),
(857, 3998),
(858, 1534),
(859, 798),
(860, 8),
(860, 10),
(860, 1777),
(860, 5),
(860, 1214),
(860, 1291),
(861, 10),
(861, 5),
(861, 8),
(861, 1291),
(861, 1214),
(863, 968),
(863, 3998),
(863, 798),
(863, 1534),
(865, 8),
(865, 10),
(865, 1214),
(865, 5),
(865, 1291),
(865, 968),
(864, 1159),
(866, 1159),
(866, 10),
(866, 8),
(866, 5),
(866, 1214),
(867, 10),
(867, 8),
(867, 5),
(867, 1214),
(867, 1541),
(625, 10),
(625, 968),
(868, 968),
(868, 5),
(870, 968),
(870, 8),
(870, 5),
(870, 10),
(870, 1291),
(870, 1214),
(871, 10),
(871, 8),
(871, 1214),
(871, 5),
(871, 4190),
(873, 968),
(876, 968),
(877, 968),
(878, 968),
(879, 10),
(879, 1214),
(879, 8),
(879, 5),
(880, 968),
(880, 10),
(880, 8),
(880, 5),
(880, 1214),
(883, 968),
(883, 10),
(883, 5),
(883, 8),
(883, 1214),
(884, 968),
(884, 10),
(885, 968),
(885, 10),
(886, 8),
(886, 10),
(886, 5),
(886, 1214),
(888, 968),
(881, 968),
(881, 8),
(881, 1214),
(881, 10),
(890, 968),
(892, 10),
(892, 1214),
(892, 8),
(892, 5),
(891, 10),
(891, 5),
(891, 8),
(891, 1214),
(893, 8),
(893, 968),
(893, 1214),
(893, 5),
(893, 10),
(896, 968),
(895, 968),
(897, 968),
(897, 5),
(897, 8),
(897, 1214),
(897, 10),
(899, 10),
(899, 1214),
(899, 8),
(899, 5),
(898, 10),
(898, 5),
(898, 8),
(898, 1214),
(900, 10),
(900, 5),
(900, 8),
(900, 1214),
(901, 10),
(901, 5),
(901, 8),
(901, 1214),
(902, 10),
(902, 5),
(902, 8),
(902, 1214),
(904, 10),
(904, 1214),
(905, 10),
(905, 1214),
(906, 10),
(907, 10),
(908, 10),
(910, 968),
(910, 1214),
(910, 10),
(910, 8),
(910, 5),
(909, 968),
(912, 968),
(911, 968),
(911, 1214),
(911, 10),
(913, 968),
(913, 5),
(913, 10),
(913, 8),
(913, 1214),
(914, 968),
(914, 10),
(915, 968),
(915, 5),
(915, 8),
(915, 10),
(915, 1214),
(918, 968),
(919, 10),
(919, 1214),
(920, 10),
(921, 968),
(921, 10),
(930, 10),
(930, 1214),
(930, 1211),
(931, 10),
(931, 1214),
(931, 1211),
(932, 10),
(932, 1214),
(935, 968),
(934, 968),
(933, 10),
(933, 22),
(933, 8),
(933, 1214),
(933, 1291),
(937, 22),
(937, 10),
(937, 8),
(943, 1540),
(943, 961),
(942, 961),
(942, 1540),
(940, 1540),
(940, 961),
(939, 961),
(939, 1540),
(944, 1540),
(944, 22),
(951, 1291),
(953, 1001),
(951, 1001),
(956, 962),
(955, 10),
(953, 1291),
(953, 10),
(956, 1291),
(956, 10),
(951, 10);

-- --------------------------------------------------------

--
-- Table structure for table `problem_types`
--

CREATE TABLE IF NOT EXISTS `problem_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Problem types' AUTO_INCREMENT=11 ;

--
-- Dumping data for table `problem_types`
--

INSERT INTO `problem_types` (`id`, `name`, `description`) VALUES
(1, 'simplebatch', 'Batch'),
(5, 'simpletext', 'Simple text'),
(6, 'reactive1', 'Reactive1'),
(7, 'batchacm', 'ACM Type'),
(8, 'batchioi', ''),
(9, 'batchioi2010', 'batchioi2010'),
(10, 'archive', 'Output only zipped');

-- --------------------------------------------------------

--
-- Table structure for table `service_requests`
--

CREATE TABLE IF NOT EXISTS `service_requests` (
  `id` int(14) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `host_ip_address` varchar(12) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `path_info` varchar(255) DEFAULT NULL,
  `parameter` text,
  `query_string` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `callback_url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=610 ;

--
-- Dumping data for table `service_requests`
--

INSERT INTO `service_requests` (`id`, `timestamp`, `host_ip_address`, `user_agent`, `path_info`, `parameter`, `query_string`, `status`, `callback_url`) VALUES
(1, '2012-03-31 17:38:53', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/update', '{"id":"1","clientid":"100","clienttoken":"100"}', 'id=1&clientid=100&clienttoken=100', 0, NULL),
(2, '2012-03-31 17:40:04', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/update', '{"id":"1","clientid":"100","clienttoken":"100","callbackurl":"http:"}', 'id=1&clientid=100&clienttoken=100&callbackurl=http:', 0, 'http:'),
(3, '2012-03-31 17:46:40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"aaa"}', 'clientid=100&clienttoken=aaa', 0, NULL),
(4, '2012-04-01 14:06:11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(5, '2012-04-01 14:07:12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100?id=5"}', 'clientid=100&clienttoken=100?id=5', 0, NULL),
(6, '2012-04-01 14:07:38', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(7, '2012-04-01 14:09:03', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(8, '2012-04-01 14:09:16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(9, '2012-04-01 14:09:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(10, '2012-04-01 14:09:59', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(11, '2012-04-01 14:10:30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(12, '2012-04-01 14:16:13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(13, '2012-04-01 14:19:02', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"5"}', 'clientid=100&clienttoken=100&id=5', 0, NULL),
(14, '2012-04-01 14:25:40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100?id=5"}', 'clientid=100&clienttoken=100?id=5', 0, NULL),
(15, '2012-04-01 14:25:47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(16, '2012-04-01 14:26:10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(17, '2012-04-01 14:26:32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(18, '2012-04-01 14:27:44', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(19, '2012-04-01 14:28:02', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(20, '2012-04-01 14:28:28', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(21, '2012-04-01 14:29:08', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(22, '2012-04-01 14:30:14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(23, '2012-04-01 14:31:00', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(24, '2012-04-01 14:32:05', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(25, '2012-04-01 14:32:33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(26, '2012-04-01 14:34:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(27, '2012-04-01 14:34:54', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(28, '2012-04-01 14:43:26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(29, '2012-04-01 14:43:30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"8"}', 'clientid=100&clienttoken=100&id=8', 0, NULL),
(30, '2012-04-01 14:43:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(31, '2012-04-01 14:43:39', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(32, '2012-04-01 14:43:46', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(33, '2012-04-01 14:43:49', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"9"}', 'clientid=100&clienttoken=100&id=9', 0, NULL),
(34, '2012-04-01 14:46:39', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(35, '2012-04-01 14:46:44', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(36, '2012-04-01 14:47:07', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(37, '2012-04-01 14:47:43', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(38, '2012-04-01 14:51:00', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(39, '2012-04-01 14:51:07', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"11"}', 'clientid=100&clienttoken=100&id=11', 0, NULL),
(40, '2012-04-01 14:51:30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(41, '2012-04-01 14:51:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(42, '2012-04-01 14:51:51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(43, '2012-04-01 14:51:57', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(44, '2012-04-01 14:54:59', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"11"}', 'clientid=100&clienttoken=100&id=11', 0, NULL),
(45, '2012-04-01 14:55:00', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(46, '2012-04-01 14:55:04', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(47, '2012-04-01 14:55:31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(48, '2012-04-01 14:55:46', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(49, '2012-04-01 14:56:02', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(50, '2012-04-01 14:58:52', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(51, '2012-04-01 14:59:04', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(52, '2012-04-01 15:01:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(53, '2012-04-01 15:01:54', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(54, '2012-04-01 15:02:06', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100","callback":"localhost"}', 'clientid=100&clienttoken=100&callback=localhost', 0, NULL),
(55, '2012-04-01 15:10:44', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(56, '2012-04-01 15:10:47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(57, '2012-04-01 15:10:56', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(58, '2012-04-01 15:11:59', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(59, '2012-04-01 15:12:10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"12"}', 'clientid=100&clienttoken=100&id=12', 0, NULL),
(60, '2012-04-01 15:13:17', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(61, '2012-04-01 15:13:25', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(62, '2012-04-01 15:14:54', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(63, '2012-04-01 15:15:11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(64, '2012-04-01 15:15:15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(65, '2012-04-01 15:15:23', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(66, '2012-04-01 15:15:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(67, '2012-04-01 15:16:19', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(68, '2012-04-01 15:16:37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(69, '2012-04-01 15:17:47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(70, '2012-04-01 15:20:24', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(71, '2012-04-01 15:23:27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(72, '2012-04-01 15:23:41', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(73, '2012-04-01 15:24:48', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(74, '2012-04-01 15:25:41', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(75, '2012-04-02 05:10:56', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(76, '2012-04-02 05:14:42', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(77, '2012-04-02 05:16:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(78, '2012-04-02 05:21:58', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(79, '2012-04-02 05:23:27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(80, '2012-04-02 05:23:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(81, '2012-04-02 05:23:55', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(82, '2012-04-02 05:25:43', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(83, '2012-04-02 05:25:50', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(84, '2012-04-02 05:26:06', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(85, '2012-04-02 05:26:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(86, '2012-04-02 05:26:54', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(87, '2012-04-02 05:29:01', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(88, '2012-04-02 05:29:18', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(89, '2012-04-02 05:30:43', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"28"}', 'clientid=100&clienttoken=100&id=28', 0, NULL),
(90, '2012-04-02 05:32:22', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(91, '2012-04-02 05:32:31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100","id":"29"}', 'clientid=100&clienttoken=100&id=29', 0, NULL),
(92, '2012-04-02 05:32:40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100","id":"29"}', 'clientid=100&clienttoken=100&id=29', 0, NULL),
(93, '2012-04-02 05:33:46', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(94, '2012-04-02 05:33:55', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(95, '2012-04-02 05:34:37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(96, '2012-04-02 05:36:16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(97, '2012-04-02 05:36:40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(98, '2012-04-02 05:36:52', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(99, '2012-04-02 05:40:43', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(100, '2012-04-02 05:40:55', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(101, '2012-04-02 05:43:13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(102, '2012-04-02 05:43:38', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(103, '2012-04-02 05:45:56', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"30"}', 'clientid=100&clienttoken=100&id=30', 0, NULL),
(104, '2012-04-02 05:48:36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(105, '2012-04-02 05:50:37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(106, '2012-04-02 05:50:57', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(107, '2012-04-02 05:52:06', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(108, '2012-04-02 05:52:31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(109, '2012-04-02 05:52:44', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(110, '2012-04-02 05:53:58', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(111, '2012-04-02 05:54:04', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(112, '2012-04-02 05:55:00', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"32"}', 'clientid=100&clienttoken=100&id=32', 0, NULL),
(113, '2012-04-02 05:55:14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"32"}', 'clientid=100&clienttoken=100&id=32', 0, NULL),
(114, '2012-04-02 05:57:40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"32"}', 'clientid=100&clienttoken=100&id=32', 0, NULL),
(115, '2012-04-02 05:57:55', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"32"}', 'clientid=100&clienttoken=100&id=32', 0, NULL),
(116, '2012-04-02 05:58:20', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"32"}', 'clientid=100&clienttoken=100&id=32', 0, NULL),
(117, '2012-04-02 05:59:13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"32"}', 'clientid=100&clienttoken=100&id=32', 0, NULL),
(118, '2012-04-02 05:59:21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(119, '2012-04-02 06:04:18', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(120, '2012-04-02 06:04:21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"31"}', 'clientid=100&clienttoken=100&id=31', 0, NULL),
(121, '2012-04-02 06:04:30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"32"}', 'clientid=100&clienttoken=100&id=32', 0, NULL),
(122, '2012-04-02 06:04:36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(123, '2012-04-02 06:05:41', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(124, '2012-04-02 06:08:36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(125, '2012-04-02 06:09:10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(126, '2012-04-02 06:09:27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(127, '2012-04-02 06:11:07', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(128, '2012-04-02 06:11:30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(129, '2012-04-02 06:11:39', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(130, '2012-04-02 06:31:46', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/recompile', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(131, '2012-06-10 18:20:56', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(132, '2012-06-10 18:21:09', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(133, '2012-06-10 18:22:28', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(134, '2012-06-10 18:22:34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(135, '2012-06-10 18:22:51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"34"}', 'clientid=100&clienttoken=100&id=34', 0, NULL),
(136, '2012-06-17 04:25:04', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(137, '2012-06-17 04:25:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(138, '2012-06-17 04:25:47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(139, '2012-06-17 04:27:12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(140, '2012-06-17 04:27:52', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(141, '2012-06-17 04:28:59', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(142, '2012-06-17 04:30:40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(143, '2012-06-17 04:40:18', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(144, '2012-06-17 04:40:49', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(145, '2012-06-17 04:41:17', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(146, '2012-06-17 04:48:26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(147, '2012-06-17 05:15:49', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(148, '2012-06-17 05:22:02', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(149, '2012-06-17 05:26:38', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(150, '2012-06-17 05:50:24', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(151, '2012-06-17 05:50:29', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(152, '2012-06-17 05:50:49', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(153, '2012-06-17 05:51:17', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(154, '2012-06-17 05:51:23', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(155, '2012-06-17 05:51:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(156, '2012-06-17 05:51:51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(157, '2012-06-17 05:52:03', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(158, '2012-06-17 05:52:37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(159, '2012-06-17 05:52:56', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(160, '2012-06-17 05:53:36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(161, '2012-06-17 05:53:50', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(162, '2012-06-17 05:55:33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/getdetail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(163, '2012-06-17 05:55:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"id":"38","clientid":"100","clienttoken":"100"}', 'id=38&clientid=100&clienttoken=100', 0, NULL),
(164, '2012-06-17 05:55:43', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"9","clientid":"100","clienttoken":"100"}', 'id=9&clientid=100&clienttoken=100', 0, NULL),
(165, '2012-06-17 05:56:14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"9","clientid":"100","clienttoken":"100"}', 'id=9&clientid=100&clienttoken=100', 0, NULL),
(166, '2012-06-17 05:56:28', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"9","clientid":"100","clienttoken":"100"}', 'id=9&clientid=100&clienttoken=100', 0, NULL),
(167, '2012-06-17 05:56:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"9","clientid":"100","clienttoken":"100"}', 'id=9&clientid=100&clienttoken=100', 0, NULL),
(168, '2012-06-17 05:56:47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(169, '2012-06-17 05:59:11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(170, '2012-06-17 05:59:14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(171, '2012-06-17 05:59:19', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(172, '2012-06-17 05:59:28', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(173, '2012-06-17 05:59:32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(174, '2012-06-17 05:59:42', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(175, '2012-06-17 05:59:48', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"id":"5","clientid":"100","clienttoken":"100"}', 'id=5&clientid=100&clienttoken=100', 0, NULL),
(176, '2012-06-21 05:46:58', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(177, '2012-06-21 05:47:03', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL),
(178, '2012-06-21 05:47:15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL),
(179, '2012-06-21 05:47:22', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL),
(180, '2012-06-21 05:49:57', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/compile', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(181, '2012-06-21 05:51:18', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(182, '2012-06-21 05:51:34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"39"}', 'clientid=100&clienttoken=100&id=39', 0, NULL),
(183, '2012-06-21 06:20:18', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"39"}', 'clientid=100&clienttoken=100&id=39', 0, NULL),
(184, '2012-06-21 06:20:19', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL),
(185, '2012-06-21 06:20:20', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"39"}', 'clientid=100&clienttoken=100&id=39', 0, NULL),
(186, '2012-06-21 07:02:32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"39"}', 'clientid=100&clienttoken=100&id=39', 0, NULL),
(187, '2012-06-21 07:02:34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL);
INSERT INTO `service_requests` (`id`, `timestamp`, `host_ip_address`, `user_agent`, `path_info`, `parameter`, `query_string`, `status`, `callback_url`) VALUES
(188, '2012-06-21 07:02:42', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL),
(189, '2012-06-21 07:04:10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL),
(190, '2012-06-21 07:43:11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"38"}', 'clientid=100&clienttoken=100&id=38', 0, NULL),
(191, '2012-07-05 04:49:59', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(192, '2012-07-06 08:23:16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(193, '2012-07-06 08:30:00', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(194, '2012-07-06 08:31:03', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(195, '2012-07-06 08:33:34', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(196, '2012-07-06 08:33:40', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(197, '2012-07-06 08:34:20', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(198, '2012-07-06 08:37:22', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(199, '2012-07-06 08:37:36', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"5"}', 'clientid=1&clienttoken=1&id=5', 0, NULL),
(200, '2012-07-06 08:37:37', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(201, '2012-07-06 08:37:39', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(202, '2012-07-06 08:37:41', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(203, '2012-07-06 08:37:43', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"7"}', 'clientid=1&clienttoken=1&id=7', 0, NULL),
(204, '2012-07-06 08:37:50', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(205, '2012-07-06 08:37:55', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"8"}', 'clientid=1&clienttoken=1&id=8', 0, NULL),
(206, '2012-07-06 08:39:07', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(207, '2012-07-06 08:39:42', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"9"}', 'clientid=1&clienttoken=1&id=9', 0, NULL),
(208, '2012-07-06 08:39:48', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"9"}', 'clientid=1&clienttoken=1&id=9', 0, NULL),
(209, '2012-07-06 08:40:21', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"9"}', 'clientid=1&clienttoken=1&id=9', 0, NULL),
(210, '2012-07-06 08:40:28', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"9"}', 'clientid=1&clienttoken=1&id=9', 0, NULL),
(211, '2012-07-06 08:40:57', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"9"}', 'clientid=1&clienttoken=1&id=9', 0, NULL),
(212, '2012-07-06 08:49:38', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"39"}', 'clientid=100&clienttoken=100&id=39', 0, NULL),
(213, '2012-07-06 08:49:41', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"1"}', 'clientid=100&clienttoken=100&id=1', 0, NULL),
(214, '2012-07-06 08:49:42', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"2"}', 'clientid=100&clienttoken=100&id=2', 0, NULL),
(215, '2012-07-06 08:49:47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/evaluationset/detail', '{"clientid":"100","clienttoken":"100","id":"9"}', 'clientid=100&clienttoken=100&id=9', 0, NULL),
(216, '2012-07-06 09:06:55', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(217, '2012-07-06 09:07:08', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(218, '2012-07-06 09:07:32', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(219, '2012-07-06 09:07:55', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(220, '2012-07-06 09:09:52', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(221, '2012-07-06 09:12:27', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(222, '2012-07-06 09:32:04', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(223, '2012-07-06 09:34:27', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(224, '2012-07-06 09:52:55', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"9"}', 'clientid=100&clienttoken=100&id=9', 0, NULL),
(225, '2012-07-06 09:52:57', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"6"}', 'clientid=100&clienttoken=100&id=6', 0, NULL),
(226, '2012-07-06 09:54:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(227, '2012-07-06 09:55:23', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(228, '2012-07-06 09:55:29', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(229, '2012-07-06 09:58:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(230, '2012-07-06 10:01:15', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"9"}', 'clientid=1&clienttoken=1&id=9', 0, NULL),
(231, '2012-07-06 10:01:50', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(232, '2012-07-06 10:02:10', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(233, '2012-07-06 10:02:50', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(234, '2012-07-06 10:03:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(235, '2012-07-06 10:03:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"6"}', 'clientid=1&clienttoken=1&id=6', 0, NULL),
(236, '2012-07-11 06:25:23', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100"}', 'clientid=100&clienttoken=100', 0, NULL),
(237, '2012-07-11 06:25:26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100?id=1"}', 'clientid=100&clienttoken=100?id=1', 0, NULL),
(238, '2012-07-11 06:27:46', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(239, '2012-07-11 06:27:51', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(240, '2012-07-11 06:27:56', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(241, '2012-07-11 06:28:01', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(242, '2012-07-11 06:28:06', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(243, '2012-07-11 06:28:11', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(244, '2012-07-11 06:28:16', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(245, '2012-07-11 07:21:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100?id=10"}', 'clientid=100&clienttoken=100?id=10', 0, NULL),
(246, '2012-07-11 07:21:56', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100?id=7"}', 'clientid=100&clienttoken=100?id=7', 0, NULL),
(247, '2012-07-11 07:22:08', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"7"}', 'clientid=100&clienttoken=100&id=7', 0, NULL),
(248, '2012-07-11 07:34:28', '127.0.0.1', NULL, 'services/evaluationset/add', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(249, '2012-07-11 07:35:20', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"9"}', 'clientid=1&clienttoken=1&id=9', 0, NULL),
(250, '2012-07-11 07:37:08', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"10"}', 'clientid=1&clienttoken=1&id=10', 0, NULL),
(251, '2012-07-11 07:48:33', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(252, '2012-07-11 07:51:24', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(253, '2012-07-11 08:16:12', '127.0.0.1', NULL, 'services/evaluationset/update', '{"clientid":"1","clienttoken":"1","id":"10"}', 'clientid=1&clienttoken=1&id=10', 0, NULL),
(254, '2012-07-11 08:20:35', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(255, '2012-07-11 08:24:08', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(256, '2012-07-11 08:25:05', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(257, '2012-07-11 08:25:06', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(258, '2012-07-11 08:25:06', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(259, '2012-07-11 08:25:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(260, '2012-07-11 08:25:50', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(261, '2012-07-11 08:26:33', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(262, '2012-07-11 08:27:11', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(263, '2012-07-11 08:27:23', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(264, '2012-07-11 08:27:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(265, '2012-07-11 08:28:30', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(266, '2012-07-11 08:29:35', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(267, '2012-07-11 08:29:48', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(268, '2012-07-11 08:30:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(269, '2012-07-11 08:30:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(270, '2012-07-11 08:30:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(271, '2012-07-11 08:31:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(272, '2012-07-11 08:32:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(273, '2012-07-11 08:32:19', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(274, '2012-07-11 08:32:49', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(275, '2012-07-11 08:33:04', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(276, '2012-07-11 08:33:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(277, '2012-07-11 08:34:31', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(278, '2012-07-11 08:34:43', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(279, '2012-07-11 08:35:00', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(280, '2012-07-11 08:35:18', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(281, '2012-07-11 08:36:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(282, '2012-07-11 08:37:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(283, '2012-07-11 08:37:30', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"20"}', 'clientid=1&clienttoken=1&id=20', 0, NULL),
(284, '2012-07-11 08:37:45', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(285, '2012-07-11 08:55:12', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(286, '2012-07-11 08:55:56', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(287, '2012-07-11 08:55:56', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(288, '2012-07-11 08:55:57', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(289, '2012-07-11 08:56:23', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"25"}', 'clientid=1&clienttoken=1&id=25', 0, NULL),
(290, '2012-07-11 08:56:52', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"25"}', 'clientid=1&clienttoken=1&id=25', 0, NULL),
(291, '2012-07-11 08:57:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"25"}', 'clientid=1&clienttoken=1&id=25', 0, NULL),
(292, '2012-07-11 08:59:43', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(293, '2012-07-11 09:00:37', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(294, '2012-07-11 09:01:24', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(295, '2012-07-11 09:01:25', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(296, '2012-07-11 09:01:25', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(297, '2012-07-11 09:02:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"30"}', 'clientid=1&clienttoken=1&id=30', 0, NULL),
(298, '2012-07-11 09:03:40', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(299, '2012-07-11 09:03:40', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(300, '2012-07-11 09:04:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"32"}', 'clientid=1&clienttoken=1&id=32', 0, NULL),
(301, '2012-07-11 09:04:52', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"32"}', 'clientid=1&clienttoken=1&id=32', 0, NULL),
(302, '2012-07-11 09:07:04', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(303, '2012-07-11 09:07:05', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(304, '2012-07-11 09:07:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(305, '2012-07-11 09:07:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(306, '2012-07-11 09:07:21', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(307, '2012-07-11 09:07:22', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(308, '2012-07-11 09:08:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(309, '2012-07-11 09:08:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(310, '2012-07-11 09:08:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(311, '2012-07-11 09:08:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(312, '2012-07-11 09:08:57', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(313, '2012-07-11 09:08:57', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(314, '2012-07-11 09:08:59', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(315, '2012-07-11 09:09:00', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(316, '2012-07-11 09:09:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(317, '2012-07-11 09:09:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(318, '2012-07-11 09:09:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(319, '2012-07-11 09:09:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(320, '2012-07-11 09:09:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"34"}', 'clientid=1&clienttoken=1&id=34', 0, NULL),
(321, '2012-07-11 09:09:49', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"33"}', 'clientid=1&clienttoken=1&id=33', 0, NULL),
(322, '2012-07-11 09:20:02', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(323, '2012-07-11 09:20:02', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(324, '2012-07-11 09:20:04', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"35"}', 'clientid=1&clienttoken=1&id=35', 0, NULL),
(325, '2012-07-11 09:20:04', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"36"}', 'clientid=1&clienttoken=1&id=36', 0, NULL),
(326, '2012-07-11 09:20:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"36"}', 'clientid=1&clienttoken=1&id=36', 0, NULL),
(327, '2012-07-11 09:35:16', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(328, '2012-07-11 09:36:28', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(329, '2012-07-11 09:36:28', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(330, '2012-07-11 09:36:28', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(331, '2012-07-11 09:36:30', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"39"}', 'clientid=1&clienttoken=1&id=39', 0, NULL),
(332, '2012-07-11 09:36:30', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"40"}', 'clientid=1&clienttoken=1&id=40', 0, NULL),
(333, '2012-07-11 09:36:47', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"40"}', 'clientid=1&clienttoken=1&id=40', 0, NULL),
(334, '2012-07-11 09:37:12', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"40"}', 'clientid=1&clienttoken=1&id=40', 0, NULL),
(335, '2012-07-11 09:37:33', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"40"}', 'clientid=1&clienttoken=1&id=40', 0, NULL),
(336, '2012-07-11 09:53:12', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"39"}', 'clientid=1&clienttoken=1&id=39', 0, NULL),
(337, '2012-07-11 09:53:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"39"}', 'clientid=1&clienttoken=1&id=39', 0, NULL),
(338, '2012-07-11 10:13:27', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(339, '2012-07-11 10:13:34', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(340, '2012-07-11 10:13:35', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(341, '2012-07-11 10:13:35', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(342, '2012-07-11 10:13:36', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"43"}', 'clientid=1&clienttoken=1&id=43', 0, NULL),
(343, '2012-07-11 10:13:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"44"}', 'clientid=1&clienttoken=1&id=44', 0, NULL),
(344, '2012-07-11 10:14:00', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"43"}', 'clientid=1&clienttoken=1&id=43', 0, NULL),
(345, '2012-07-11 10:14:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"44"}', 'clientid=1&clienttoken=1&id=44', 0, NULL),
(346, '2012-07-11 10:14:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"43"}', 'clientid=1&clienttoken=1&id=43', 0, NULL),
(347, '2012-07-11 10:14:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"44"}', 'clientid=1&clienttoken=1&id=44', 0, NULL),
(348, '2012-07-11 10:15:10', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"43"}', 'clientid=1&clienttoken=1&id=43', 0, NULL),
(349, '2012-07-11 10:15:10', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"44"}', 'clientid=1&clienttoken=1&id=44', 0, NULL),
(350, '2012-07-12 05:49:47', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(351, '2012-07-12 05:50:52', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(352, '2012-07-12 06:13:53', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(353, '2012-07-12 06:14:17', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(354, '2012-07-12 06:21:12', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(355, '2012-07-12 06:21:12', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(356, '2012-07-12 06:21:49', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(357, '2012-07-12 06:21:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(358, '2012-07-12 06:21:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(359, '2012-07-12 06:22:13', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(360, '2012-07-12 06:22:14', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(361, '2012-07-12 06:23:14', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(362, '2012-07-12 06:23:14', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(363, '2012-07-12 06:23:24', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(364, '2012-07-12 06:23:24', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(365, '2012-07-12 06:27:54', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(366, '2012-07-12 06:28:06', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(367, '2012-07-12 06:28:14', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(368, '2012-07-12 06:28:14', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(369, '2012-07-12 06:28:34', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(370, '2012-07-12 06:28:34', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(371, '2012-07-12 06:28:34', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(372, '2012-07-12 06:29:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(373, '2012-07-12 06:29:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(374, '2012-07-12 06:30:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(375, '2012-07-12 06:30:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(376, '2012-07-12 06:31:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(377, '2012-07-12 06:31:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(378, '2012-07-12 06:32:46', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(379, '2012-07-12 06:33:58', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"51"}', 'clientid=1&clienttoken=1&id=51', 0, NULL),
(380, '2012-07-12 06:39:45', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(381, '2012-07-12 06:39:45', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(382, '2012-07-12 06:39:45', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(383, '2012-07-12 06:40:17', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(384, '2012-07-12 06:40:17', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(385, '2012-07-12 06:40:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(386, '2012-07-12 06:40:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(387, '2012-07-12 06:46:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(388, '2012-07-12 06:46:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(389, '2012-07-12 06:55:23', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"48"}', 'clientid=1&clienttoken=1&id=48', 0, NULL),
(390, '2012-07-12 06:55:23', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"48"}', 'clientid=1&clienttoken=1&id=48', 0, NULL),
(391, '2012-07-12 06:55:23', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(392, '2012-07-12 06:55:24', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(393, '2012-07-12 06:55:24', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"51"}', 'clientid=1&clienttoken=1&id=51', 0, NULL),
(394, '2012-07-12 06:55:24', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"51"}', 'clientid=1&clienttoken=1&id=51', 0, NULL),
(395, '2012-07-12 06:55:25', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(396, '2012-07-12 06:55:25', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(397, '2012-07-12 06:55:25', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(398, '2012-07-12 06:55:25', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(399, '2012-07-12 06:55:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(400, '2012-07-12 06:55:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(401, '2012-07-12 06:55:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(402, '2012-07-12 06:55:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(403, '2012-07-12 06:55:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(404, '2012-07-12 06:55:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(405, '2012-07-12 06:55:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(406, '2012-07-12 06:55:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(407, '2012-07-12 06:55:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(408, '2012-07-12 06:55:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(409, '2012-07-12 06:55:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(410, '2012-07-12 06:55:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(411, '2012-07-12 06:55:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(412, '2012-07-12 06:55:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(413, '2012-07-12 06:57:38', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"48"}', 'clientid=1&clienttoken=1&id=48', 0, NULL),
(414, '2012-07-12 06:57:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"48"}', 'clientid=1&clienttoken=1&id=48', 0, NULL),
(415, '2012-07-12 06:57:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(416, '2012-07-12 06:57:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(417, '2012-07-12 06:57:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"51"}', 'clientid=1&clienttoken=1&id=51', 0, NULL),
(418, '2012-07-12 06:57:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"51"}', 'clientid=1&clienttoken=1&id=51', 0, NULL),
(419, '2012-07-12 06:57:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(420, '2012-07-12 06:57:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(421, '2012-07-12 06:57:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(422, '2012-07-12 06:57:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(423, '2012-07-12 06:57:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(424, '2012-07-12 06:57:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(425, '2012-07-12 06:57:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(426, '2012-07-12 06:57:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(427, '2012-07-12 06:57:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(428, '2012-07-12 06:57:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(429, '2012-07-12 06:57:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(430, '2012-07-12 06:57:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(431, '2012-07-12 06:57:43', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(432, '2012-07-12 06:57:43', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(433, '2012-07-12 06:57:43', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(434, '2012-07-12 06:57:43', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(435, '2012-07-12 06:57:43', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(436, '2012-07-12 06:57:44', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(437, '2012-07-12 06:57:57', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(438, '2012-07-12 06:58:34', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"48"}', 'clientid=1&clienttoken=1&id=48', 0, NULL),
(439, '2012-07-12 06:58:35', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"48"}', 'clientid=1&clienttoken=1&id=48', 0, NULL),
(440, '2012-07-12 06:58:35', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(441, '2012-07-12 06:58:35', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"50"}', 'clientid=1&clienttoken=1&id=50', 0, NULL),
(442, '2012-07-12 06:58:36', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"51"}', 'clientid=1&clienttoken=1&id=51', 0, NULL),
(443, '2012-07-12 06:58:36', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"51"}', 'clientid=1&clienttoken=1&id=51', 0, NULL),
(444, '2012-07-12 06:58:36', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(445, '2012-07-12 06:58:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(446, '2012-07-12 06:58:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(447, '2012-07-12 06:58:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(448, '2012-07-12 06:58:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(449, '2012-07-12 06:58:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(450, '2012-07-12 06:58:38', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(451, '2012-07-12 06:58:38', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(452, '2012-07-12 06:58:38', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(453, '2012-07-12 06:58:38', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(454, '2012-07-12 06:58:38', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(455, '2012-07-12 06:58:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(456, '2012-07-12 06:58:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(457, '2012-07-12 06:58:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(458, '2012-07-12 06:58:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(459, '2012-07-12 06:58:39', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(460, '2012-07-12 06:58:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(461, '2012-07-12 06:58:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(462, '2012-07-12 06:58:45', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(463, '2012-07-12 06:59:13', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"47"}', 'clientid=1&clienttoken=1&id=47', 0, NULL),
(464, '2012-07-12 06:59:13', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"46"}', 'clientid=1&clienttoken=1&id=46', 0, NULL),
(465, '2012-07-12 06:59:14', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"49"}', 'clientid=1&clienttoken=1&id=49', 0, NULL),
(466, '2012-07-12 06:59:57', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(467, '2012-07-12 07:00:21', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(468, '2012-07-12 07:00:25', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(469, '2012-07-12 07:00:30', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(470, '2012-07-12 07:00:30', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(471, '2012-07-12 07:00:51', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(472, '2012-07-12 07:00:51', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(473, '2012-07-12 07:01:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(474, '2012-07-12 07:01:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(475, '2012-07-12 07:01:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(476, '2012-07-12 07:01:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(477, '2012-07-12 07:01:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(478, '2012-07-12 07:01:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(479, '2012-07-12 07:01:17', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(480, '2012-07-12 07:01:17', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(481, '2012-07-12 07:02:00', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(482, '2012-07-12 07:02:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(483, '2012-07-12 07:02:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(484, '2012-07-12 07:02:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(485, '2012-07-12 07:02:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL);
INSERT INTO `service_requests` (`id`, `timestamp`, `host_ip_address`, `user_agent`, `path_info`, `parameter`, `query_string`, `status`, `callback_url`) VALUES
(486, '2012-07-12 07:02:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(487, '2012-07-12 07:02:35', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(488, '2012-07-12 07:02:35', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(489, '2012-07-12 07:03:04', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(490, '2012-07-12 07:03:04', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(491, '2012-07-12 07:03:06', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(492, '2012-07-12 07:03:07', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(493, '2012-07-12 07:03:25', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(494, '2012-07-12 07:03:25', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(495, '2012-07-12 07:03:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(496, '2012-07-12 07:03:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(497, '2012-07-12 07:05:11', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"54"}', 'clientid=1&clienttoken=1&id=54', 0, NULL),
(498, '2012-07-12 07:05:11', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"53"}', 'clientid=1&clienttoken=1&id=53', 0, NULL),
(499, '2012-07-12 07:13:00', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(500, '2012-07-12 07:13:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(501, '2012-07-12 07:13:15', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(502, '2012-07-12 07:13:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(503, '2012-07-12 07:13:46', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(504, '2012-07-12 07:17:13', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(505, '2012-07-12 07:17:25', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(506, '2012-07-12 07:17:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(507, '2012-07-12 07:17:51', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(508, '2012-07-12 07:18:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(509, '2012-07-12 07:18:16', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(510, '2012-07-12 07:18:33', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(511, '2012-07-12 07:18:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(512, '2012-07-12 07:18:46', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(513, '2012-07-12 07:18:57', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(514, '2012-07-12 07:19:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(515, '2012-07-12 07:19:08', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(516, '2012-07-12 07:19:20', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(517, '2012-07-12 07:19:24', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(518, '2012-07-12 07:20:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(519, '2012-07-12 07:20:13', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(520, '2012-07-12 07:20:30', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(521, '2012-07-12 07:20:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(522, '2012-07-12 07:23:24', '127.0.0.1', NULL, 'services/grading/grade', '{"clientid":"1","clienttoken":"1"}', 'clientid=1&clienttoken=1', 0, NULL),
(523, '2012-07-12 07:24:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(524, '2012-07-12 08:11:41', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(525, '2012-07-12 08:17:14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(526, '2012-07-12 08:18:47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(527, '2012-07-12 08:20:44', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(528, '2012-07-12 08:20:55', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(529, '2012-07-12 08:22:15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(530, '2012-07-12 08:22:27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(531, '2012-07-12 08:23:21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(532, '2012-07-12 08:29:28', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(533, '2012-07-12 08:30:38', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(534, '2012-07-12 08:33:04', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/grade', '{"clientid":"100","clienttoken":"100","flat":"1"}', 'clientid=100&clienttoken=100&flat=1', 0, NULL),
(535, '2012-07-16 07:58:47', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(536, '2012-07-16 08:02:20', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(537, '2012-07-16 08:02:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(538, '2012-07-16 08:04:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(539, '2012-07-16 08:04:04', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(540, '2012-07-16 08:04:08', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(541, '2012-07-16 08:12:17', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"39"}', 'clientid=100&clienttoken=100&id=39', 0, NULL),
(542, '2012-07-16 08:12:34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"56"}', 'clientid=100&clienttoken=100&id=56', 0, NULL),
(543, '2012-07-16 08:13:08', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"57"}', 'clientid=100&clienttoken=100&id=57', 0, NULL),
(544, '2012-07-16 08:13:33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/detail', '{"clientid":"100","clienttoken":"100","id":"56"}', 'clientid=100&clienttoken=100&id=56', 0, NULL),
(545, '2012-07-16 08:18:13', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(546, '2012-07-16 08:18:13', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(547, '2012-07-16 08:18:14', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(548, '2012-07-16 08:18:14', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(549, '2012-07-16 08:19:18', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"56"}', 'clientid=100&clienttoken=100&id=56', 0, NULL),
(550, '2012-07-16 08:20:34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7', 'services/grading/regrade', '{"clientid":"100","clienttoken":"100","id":"56"}', 'clientid=100&clienttoken=100&id=56', 0, NULL),
(551, '2012-07-16 08:20:45', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(552, '2012-07-16 08:20:45', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(553, '2012-07-16 08:20:46', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(554, '2012-07-16 08:20:46', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(555, '2012-07-16 08:23:17', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(556, '2012-07-16 08:23:17', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(557, '2012-07-16 08:23:18', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(558, '2012-07-16 08:23:18', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(559, '2012-07-16 08:24:09', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(560, '2012-07-16 08:25:36', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(561, '2012-07-16 08:25:36', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(562, '2012-07-16 08:25:37', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(563, '2012-07-16 08:25:37', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(564, '2012-07-16 08:27:40', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(565, '2012-07-16 08:27:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(566, '2012-07-16 08:27:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(567, '2012-07-16 08:27:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(568, '2012-07-16 08:27:40', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(569, '2012-07-16 08:27:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(570, '2012-07-16 08:27:41', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(571, '2012-07-16 08:27:41', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(572, '2012-07-16 08:27:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(573, '2012-07-16 08:27:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(574, '2012-07-16 08:27:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(575, '2012-07-16 08:27:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(576, '2012-07-16 08:27:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(577, '2012-07-16 08:27:42', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(578, '2012-07-16 08:28:01', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(579, '2012-07-16 08:28:01', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(580, '2012-07-16 08:28:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(581, '2012-07-16 08:28:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(582, '2012-07-16 08:28:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(583, '2012-07-16 08:28:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(584, '2012-07-16 08:28:02', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(585, '2012-07-16 08:28:02', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(586, '2012-07-16 08:28:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(587, '2012-07-16 08:28:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(588, '2012-07-16 08:28:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(589, '2012-07-16 08:28:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(590, '2012-07-16 08:28:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(591, '2012-07-16 08:28:03', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(592, '2012-07-16 08:28:26', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(593, '2012-07-16 08:28:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(594, '2012-07-16 08:28:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(595, '2012-07-16 08:28:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(596, '2012-07-16 08:28:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(597, '2012-07-16 08:28:26', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(598, '2012-07-16 08:28:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(599, '2012-07-16 08:28:27', '127.0.0.1', NULL, 'services/grading/regrade', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(600, '2012-07-16 08:28:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(601, '2012-07-16 08:28:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(602, '2012-07-16 08:28:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(603, '2012-07-16 08:28:27', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(604, '2012-07-16 08:28:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(605, '2012-07-16 08:28:28', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(606, '2012-07-16 08:28:31', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(607, '2012-07-16 08:28:31', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL),
(608, '2012-07-16 08:41:22', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"55"}', 'clientid=1&clienttoken=1&id=55', 0, NULL),
(609, '2012-07-16 08:41:22', '127.0.0.1', NULL, 'services/grading/detail', '{"clientid":"1","clienttoken":"1","id":"56"}', 'clientid=1&clienttoken=1&id=56', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL,
  `submitter_id` int(10) unsigned NOT NULL,
  `contest_id` int(10) unsigned DEFAULT NULL,
  `chapter_id` int(10) unsigned DEFAULT NULL,
  `submitted_time` datetime NOT NULL,
  `submit_content` text NOT NULL,
  `grade_time` datetime DEFAULT NULL,
  `grade_content` text,
  `grade_output` longtext,
  `grade_status` int(11) DEFAULT NULL,
  `verdict` text,
  `score` float NOT NULL,
  `comment` text NOT NULL,
  `file` longblob,
  PRIMARY KEY (`id`),
  KEY `submitter_id` (`submitter_id`,`contest_id`),
  KEY `contest_id` (`contest_id`),
  KEY `submitted_time` (`submitted_time`),
  KEY `grade_status` (`grade_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_time` datetime NOT NULL,
  `creator_id` int(10) unsigned NOT NULL,
  `first_chapter_id` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `first_chapter_id` (`first_chapter_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `name`, `description`, `created_time`, `creator_id`, `first_chapter_id`, `status`) VALUES
(2, 'TOKI Training Gate - BETA', '&nbsp;', '2011-01-22 22:51:58', 8, 21, 1),
(3, 'Tes', 'tes', '2011-02-17 12:38:49', 8, 44, 1),
(4, 'Coba 1', '<b>coba 1</b>', '2012-01-27 21:11:15', 22, 46, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` char(50) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `last_ip` text,
  `full_name` tinytext NOT NULL,
  `join_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `site_url` tinytext,
  `institution` tinytext,
  `institution_address` tinytext,
  `institution_phone` tinytext,
  `address` tinytext NOT NULL,
  `postal_code` tinytext NOT NULL,
  `city` tinytext NOT NULL,
  `handphone` tinytext,
  `phone` tinytext,
  `active` tinyint(1) NOT NULL,
  `activation_code` varchar(32) DEFAULT NULL,
  `additional_information` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5009 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `type`, `logins`, `last_login`, `last_activity`, `last_ip`, `full_name`, `join_time`, `site_url`, `institution`, `institution_address`, `institution_phone`, `address`, `postal_code`, `city`, `handphone`, `phone`, `active`, `activation_code`, `additional_information`) VALUES
(1, 'petra.barus@gmail.com', 'admin', 'dd94709528bb1c83d08f3088d4043f4742891f4f', 0, 545, '2012-07-13 16:28:00', '2012-07-13 19:30:16', '127.0.0.1', 'Administrator', '2012-07-13 12:30:16', '', '', '', 'ITB', '', '', '', NULL, NULL, 1, '0lnCGe3ZIrXWjTvb81SKzXBCTIgLwr6r', NULL),
(2, 'me@van-odin.net', 'petra', '599162b784a67f6b18ed563e4f820ee2fee73c7e', 0, 33, '2011-10-12 15:06:29', '2011-10-12 15:07:10', '167.205.34.23', 'Petra Novandi', '2011-10-12 07:07:10', '123123123123', '123123123123', '123123a123123123123', '123123123123123132', '', '', '123132123132123123', NULL, NULL, 1, NULL, ''),
(4, 'petra.barus@ymail.com', 'petra2', '1ac273ce29cdaf50160fda8c49f6430240cf2a1c', 0, 31, '2011-03-15 10:08:03', '2011-03-15 10:08:07', '167.205.34.124', 'Petra Novandi', '2011-03-15 02:08:07', '', '', '', '', '', '', '', NULL, NULL, 0, NULL, NULL),
(5, 'brian@microbrainx.net', 'microbrain', 'a8cb92af1c0e76d4b355db0e936b8b77b4ef1e79', 0, 208, '2012-01-26 08:32:38', '2012-01-27 09:28:08', '202.51.120.85', 'Brian Marshal', '2012-01-27 01:28:08', '', 'TOKI, BPK PENABUR, NTU Singapore', '', '', '', '', '', NULL, '', 0, NULL, 'Online'),
(6, 'hallucinogenplus@yahoo.co.id', 'hallucinogen', '35576da9e79fb97909974a866ee8ba6dd5f07fd3', 0, 208, '2012-01-28 09:48:27', '2012-01-28 10:03:20', '192.168.1.115', 'Listiarso Wastuargo', '2012-01-28 02:03:20', '', '', '', '', '', '', '', NULL, NULL, 0, '98KE7VDgHXHM1keha1OQgy71oBx2iO5w', NULL),
(7, 'ilhamwk@gmail.com', 'ilham', '2fb5e13419fc89246865e7a324f476ec624e8740', 0, 76, '2012-01-26 20:25:06', '2012-01-26 20:39:52', '110.138.62.229', 'Ilham', '2012-01-26 12:39:52', '', '', '', '', '', '', '', NULL, NULL, 0, NULL, NULL),
(8, 'karoldanutama@gmail.com', 'karol', '4d28be28a3f106d176f375b1cdc16bebebbef794', 0, 611, '2012-07-13 16:27:44', '2012-07-13 16:27:55', '127.0.0.1', 'Karol Danutama', '2012-07-13 09:27:55', 'TEST', '', '', '', '53453535345345', '', '', NULL, '43543', 0, NULL, 'ITB'),
(9, 'lennie_2nd@yahoo.com', 'lennie_2nd', '17a383e57c6a68723c5afda1d849108c8f76517f', 0, 29, '2011-10-06 11:53:58', '2011-10-06 12:30:35', '128.12.121.182', 'Angelina Veni', '2011-10-06 04:30:35', '', '', '', '', '', '', '', NULL, NULL, 0, NULL, NULL),
(3, 'adi@stei.itb.ac.id', 'adi', '842e23b58315c533026d4d9cdd67a544cfb0a228', 0, 2, '2010-12-21 20:32:59', '2010-12-21 20:34:40', '110.137.93.245', 'Adi Mulyanto', '2010-12-21 12:34:40', '', '', '', '', '', '', '', NULL, NULL, 0, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
