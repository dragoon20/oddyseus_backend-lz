<?php

Yii::import("ext.evaluator.types.simple.SimpleEvaluator");
Yii::import("ext.compiler.archive.*");
Yii::import("ext.grader.simple.*");

class SimpleEvaluateCommand extends CConsoleCommand {

    public function run($args) {
        /*$data = array(
            'memorylimit' => 16000000,
            'timelimit' => 3000,
            'testcaseno' => 2
        );
        file_put_contents("/home/karol/Documents/Projects/lx-test/tc/config.json", json_encode($data));*/
        
        /*$evaluator = new SimpleEvaluator("" , $args[0] , $args[1] , $args[2]);
        $evaluator->doCompile();
        $evaluator->doExecute();
        $evaluator->cleanup();
        
        print_r($evaluator->getResult());*/
        //print_r(Yii::app()->params['config']['compiler']['temp_dir']);
       
        $model = GradeRequest::model()->findByPK(34);
        
        echo $model->evaluationset->grader_type_id . " " . $model->evaluationset->compiler_type_id . "\n";
        
        $compilerClass = ucfirst($model->evaluationset->compiler_type_id) . "Compiler";
        $graderClass = ucfirst($model->evaluationset->grader_type_id) . "Grader";
        
        $c = new $compilerClass($model);
        $c->setTarget("compile-test");
        $c->compile();
        
        $g = new $graderClass($c);
        $g->grade();
        print_r($g->getGradeResult());
    }

}

?>
