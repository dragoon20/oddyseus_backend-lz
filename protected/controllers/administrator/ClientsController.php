<?php

class ClientsController extends CAdminController {
    
    private $_model;

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Client', array(
                    'pagination' => array(
                        'pageSize' => 20,
                    ),
                ));
        $this->render('index', array('dataProvider' => $dataProvider));
    }
    
    public function actionCreate() {
        $model = new Client('create');

        if (isset($_POST['Client'])) {
            $model->attributes = $_POST['Client'];
            $model->regenerateToken();
            if ($model->validate()) {
                $model->save(false);
                $this->redirect($this->createUrl('index'));
            }
        }

        $this->render('create' , array('model' => $model));
    }
    
    public function actionDelete() {
        $model = $this->loadModel();
        $model->delete();
    }
    
    public function actionUpdate() {
        $model = $this->loadModel();
        $model->regenerateToken();
        $model->save();
        $this->redirect($this->createUrl('index'));
    }
    
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id']))
                $this->_model = Client::model()->findbyPk($_GET['id']);
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }

}

?>
