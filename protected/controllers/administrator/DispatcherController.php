<?php


class DispatcherController extends CAdminController {

    const PID_FILE = "bin/JDispatcher/JDispatcher.pid";
    const SH_PATH = "bin/JDispatcher/execute.sh";
    const CONFIG_PATH = "bin/JDispatcher/conf/default.conf";

    private $pid;

    public function init() {
        $this->pid = (file_exists(self::PID_FILE)) ? file_get_contents(self::PID_FILE) : NULL;
        if (!$this->isPsExists($this->pid))
            $this->pid = NULL;
       	parent::init();
    }

    public function getPid() {
        return $this->pid;
    }

    public function actionIndex() {
        if ($_POST) {
            $handle = fopen(self::CONFIG_PATH , "w");
            $prop = new Properties();
            foreach ($_POST['Prop'] as $key => $value) {
                //echo $key . " " . $value . "\n";
                $prop->setProperty($key , $value);
            }
            
            ///Create empty field for machines
            for ($i = 1 ; $i <= $_POST['Prop']['machine-count'] ; $i++) {
                if (!isset($_POST['Prop']['machine-address'.$i])) {
                    $prop->setProperty('machine-address'.$i , "localhost");
                }
            }
            
            $i = 1;
            $found = false;
            do {
                $found = isset($_POST['Prop']['machine-address' . $i]);
                if ($found && $i > $_POST["Prop"]["machine-count"]) {
                    $prop->removeProperty('machine-address' . $i);
                }
                $i++;
            } while ($found);
            
            $prop->store($handle);
            fclose($handle);
        }
        
        $handle = fopen(self::CONFIG_PATH , "r"); 
        $p = new Properties();
        $p->load($handle);

        $this->render('index' , array('prop' => $p));
        fclose($handle);
    }

    protected function isPsExists($pid) {
        return file_exists("/proc/" . $pid);
    }

    public function actionStart() {
        if ($this->pid == NULL) {
            chdir("bin/JDispatcher");
            exec("/usr/bin/java -jar JDispatcher.jar > log.out 2>&1 & echo $!", $out, $ret);
            if ($ret == 0) {
                file_put_contents("JDispatcher.pid", $out[0]);
            }
        }

        $this->redirect($this->createUrl("index"));
    }

    protected function parseProperties($txtProperties) {
        $result = array();

        $lines = split("\n", $txtProperties);
        $key = "";

        $isWaitingOtherLine = false;
        foreach ($lines as $i => $line) {

            if (empty($line) || (!$isWaitingOtherLine && strpos($line, "#") === 0))
                continue;

            if (!$isWaitingOtherLine) {
                $key = substr($line, 0, strpos($line, '='));
                $value = substr($line, strpos($line, '=') + 1, strlen($line));
            } else {
                $value .= $line;
            }

            /* Check if ends with single '\' */
            if (strrpos($value, "\\") === strlen($value) - strlen("\\")) {
                $value = substr($value, 0, strlen($value) - 1) . "\n";
                $isWaitingOtherLine = true;
            } else {
                $isWaitingOtherLine = false;
            }

            $result[$key] = $value;
            unset($lines[$i]);
        }

        return $result;
    }

    public function actionKill() {
        if ($this->pid != NULL) {
            exec("kill -9 " . $this->pid);
        }

        $this->redirect($this->createUrl("index"));
    }

}

?>
