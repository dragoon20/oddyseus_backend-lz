<?php

class BackendController extends CController {

    protected function printToLog($model) {
        $handle = fopen("log/lx.log" , "a+");
        fprintf($handle , "[%s] Server %s received job for request #$model->id\n" , date("d/m/y h:i:s") , $_SERVER["SERVER_ADDR"]);
        fclose($handle);
    }
    
    public function actionGrade() {
//        $load = $this->getServerLoad();
//        if ($this->getCpuLoad($load['cpu']) > 80.00) {
//            throw new CHttpException(413, "Offer rejected");
//        }
        if (isset($_GET['id']) && ($model = GradeRequest::model()->findByPK($_GET['id'])) != null) {
            $this->printToLog($model);
            $model->report = "";
            echo "<pre>";

            //echo $model->evaluationset->grader_type_id . " " . $model->evaluationset->compiler_type_id . "\n";

            $compilerClass = ucfirst($model->evaluationset->compiler_type_id) . "Compiler";
            Yii::import("ext.compiler." . $model->evaluationset->compiler_type_id . ".*");
            
            if ($model->action != GradeRequest::ACTION_EXECUTE) {
                $graderClass = ucfirst($model->evaluationset->grader_type_id) . "Grader";
                Yii::import("ext.grader." . $model->evaluationset->grader_type_id . ".*");
            }
            else {
                $graderClass = "SimpleGrader";
                Yii::import("ext.grader.simple.*");
            }
            $c = new $compilerClass($model);
            $c->setTarget("compile-test");
            
            $capabilities = explode(";", $model->evaluationset->languages);
            foreach ($capabilities as $cap)
            {
				$c->addCapabilities($cap);
			}
            
            $c->compile();

            $g = new $graderClass($c);
            if ($model->action == GradeRequest::ACTION_EXECUTE) {
                $g->useProvidedInput = true;
		}
            $g->grade();
            //print_r($g->getGradeResult());
            print_r(json_decode($model->report, true));
            echo "</pre>";
        }
        else
            throw new CHttpException(412, "Parameter validation failed");
    }

    private function getServerLoad() {
        exec("top -b -n1 | grep Cpu" , $output , $ret);
        $output = implode("\n", $output);
        $cpu = $output;
        
        exec("top -b -n1 | grep Mem" , $output , $ret);
        $output = implode("\n", $output);
        $mem = $output;
        return array('cpu' => $cpu , 'mem' => $mem);
    }
    
    private function getCpuLoad($cpuload) {
        $cpuload = str_replace("Cpu(s): ", "", $cpuload);
        $cpurows = explode(", " , $cpuload);
        return str_replace("%us" , "" , $cpurows[0]) . "\n";
    }

    public function actionTest() {
        $load = $this->getServerLoad();
        $cpuload = $load['cpu'];
        
        $cpuload = str_replace("Cpu(s): ", "", $cpuload);
        $cpurows = explode(", " , $cpuload);
        echo str_replace("%us" , "" , $cpurows[0]) . "\n";
        
        $memload = $load["mem"];
        $memload = str_replace("Mem: ", "", $memload);
        $memload = str_replace("k total", "", $memload);
        $memload = str_replace("k used", "", $memload);
        $memrows = explode(", " , $memload);
        echo $memrows[1] / $memrows[0] * 100;
    }

}

?>
