<?php

/**
 * Kelas penyedia layanan evaluationset 
 */
class EvaluationsetController extends CServiceController {

    private $model;
    const EVALUATIONSET_PATH = "/var/lz/evaluationsets/";

    /**
     * Operasi menambahkan EvaluationSet ke basis data 
     * @param attribute, value
     */
    public function actionAdd() {
        $this->model = new EvaluationSet();

        $this->updateModel();

        $this->setReplyAttribute('evaluationsetid', $this->model->id);
    }

    /**
     * Operasi mengubah EvaluationSet ke basis data 
     * @param id
     * @param attribute, value
     */
    public function actionUpdate() {
        $this->loadModel();

        $this->updateModel();
    }

    /**
     * Operasi menghapus EvaluationSet dari basis data
     * @param id
     */
    public function actionDelete() {
        $this->loadModel();

        $this->model->delete();

        $this->setReply(true, "Ok");
    }

    /**
     * Operasi mencari EvaluationSet dari basis data
     * @param query
     * @return list of EvaluationSet id 
     */
    public function actionSearch() {
        
    }

    /**
     * Operasi mengambil detil Evaluation set
     * @param id
     * @return detail 
     */
    public function actionDetail() {
        $this->loadModel();

        $retval = $this->model->attributes;
        $retval['config'] = json_decode($retval['config'], true);
        $retval["file"] = null;

        $this->setReply(true, "Record found");
        $this->reply['detail'] = $retval;
//        echo "<pre>";
//        print_r($retval);
//        echo "</pre>";
    }

    /**
     * Mengambil model dengan parameter id dari basis data
     * @param id
     * @return type record
     * @throws CHttpException jika record tidak ditemukan
     */
    protected function loadModel() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $this->model = EvaluationSet::model()->findByPk($id);
            if ($this->model === null)
                throw new CHttpException(401, "No record found");
            else {
                return $this->model;
            }
        }
        else
            throw new CHttpException(401, "No record found");
    }

    /**
     * Mengupdate sebuah record dari POST parameter 
     */
    protected function updateModel() {
        $evaluationset = $this->model;
        if ($evaluationset !== null && isset($_POST['EvaluationSet'])) {
            $evaluationset->setAttributes($_POST["EvaluationSet"]);
            if ($evaluationset->validate()) {
                $this->setReply(true, "Ok");
                $evaluationset->setConfigs($_POST['config']);
                $evaluationset->client_id = $this->clientId;
                
                if (isset($_FILES['EvaluationSet']['tmp_name']['file']) && $_FILES['EvaluationSet']['tmp_name']['file'] !== "") {
                    $evaluationset->file = file_get_contents($_FILES['EvaluationSet']['tmp_name']['file']);
                }
                
                $evaluationset->save();
            } else {
                $this->setReply(false, $evaluationset->getErrors());
            }
        }
    }

}

?>
