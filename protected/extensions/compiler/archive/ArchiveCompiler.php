<?php

Yii::import("application.components.evaluators.CompilerBase");

class ArchiveCompiler extends CompilerBase {

    private $target;
    private $tmpDir;

    public function __construct($request) {
        parent::__construct($request);
    }

    /**
     * Membersihkan berkas-berkas temporer dari disk 
     */
    protected function afterCompile() {
        
    }

    protected function beforeCompile() {
        // TODO escape in model
        $this->targetFilePath = $this->getGradingDirectory() . "/" . $this->requestModel->source_file;
        file_put_contents($this->targetFilePath , $this->requestModel->file);
        
        ///Unzipping
        $tmpDir = $this->gradingDirectory;
        exec("unzip -o " . escapeshellarg($this->targetFilePath) . " -d $tmpDir");
    }

    /**
     * Membuat berkas script (.sh) yang dapat dipanggil untuk kemudian dieksekusi oleh grader
     */
    protected function generateExecutionScript() {
        if ($this->compileReturnValue == 0) 
        {
            $this->executionScriptPath = $this->gradingDirectory . "/execute.sh";
            $executionCommand = "make -s --no-print-directory grade OUTPUT=$this->gradingDirectory/Main -f $this->gradingDirectory/Makefile -C $this->gradingDirectory";
            file_put_contents($this->executionScriptPath, $executionCommand);
            chmod($this->executionScriptPath,0777);
        } else {
            $this->executionScriptPath = "";
        }
    }

    /**
     * Mengeset target dari Makefile yang diinginkan
     * @param string $target 
     */
    public function setTarget($target) {
        $this->target = $target;
    }

	public function addCapabilities($cap)
    {
	}

    /**
     * Melakukan kompilasi, akan mengeset compileReturnValue dengan nilai kembalian proses kompilasi. 0 jika sukses, selain itu gagal.
     * Pesan kompilasi dapat diambil dari compileOutput
     * Program hasil kompilasi dapat diambil di outputFilePath
     * I.S: Direktori temporer sudah dibuat dengan Makefile di dalamnya. Jika tidak ada Makefile, return value != 0
     * F.S: compileOutput, compileReturnValue, outputFilePath terisi 
     */
    protected function doCompile() {
        $tmpDir = $this->gradingDirectory;

        ///Injecting grader tester file
        //$testerFileName = $this->requestModel->evaluationset->getConfig("testerfilename");
        //file_put_contents($this->gradingDirectory . "/" . $testerFileName, $this->requestModel->evaluationset->file);

        ///Do compile process!
        //exec("make $this->target OUTPUT=$tmpDir/Main TESTER=$testerFileName -f $tmpDir/Makefile -C $tmpDir 2>&1", $out, $return);
        mkdir("$tmpDir/Main", 0777);
        exec("make $this->target OUTPUT=$tmpDir/Main -f $tmpDir/Makefile -C $tmpDir 2>&1", $out, $return);
        
        $this->compileOutput = implode("\n", $out);
        $this->compileReturnValue = $return;

        if ($return == 0) {
            $this->outputFilePath = $tmpDir . "/Main";
        }
        else {
            $this->outputFilePath = "";
        }
        $this->recursiveChmod($this->outputFilePath, 0777, 0777);
    }

    private function recursiveChmod ($path, $filePerm=0644, $dirPerm=0755)
    {
    	// Check if the path exists
    	if (!file_exists($path)) {
    		return(false);
    	}
    
    	// See whether this is a file
    	if (is_file($path)) {
    		// Chmod the file with our given filepermissions
    		chmod($path, $filePerm);
    		// If this is a directory...
    	} elseif (is_dir($path)) {
    		// Then get an array of the contents
    		$foldersAndFiles = scandir($path);
    
    		// Remove "." and ".." from the list
    		$entries = array_slice($foldersAndFiles, 2);
    
    		// Parse every result...
    		foreach ($entries as $entry) {
    			// And call this function again recursively, with the same permissions
    			$this->recursiveChmod($path."/".$entry, $filePerm, $dirPerm);
    		}
    
    		// When we are done with the contents of the directory, we chmod the directory itself
    		chmod($path, $dirPerm);
    	}
    
    	// Everything seemed to work out well, return true
    	return(true);
    }
    
}

?>
