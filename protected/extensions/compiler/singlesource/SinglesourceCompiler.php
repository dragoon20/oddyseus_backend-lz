<?php

Yii::import("application.components.evaluators.CompilerBase");

class SinglesourceCompiler extends CompilerBase 
{
	private $language;
    private $extension;
    private $type;
    private $capabilities = array(
								'C'=>false,
								'C++'=>false,
								'Java'=>false,
								'LISP'=>false,
								'Pascal'=>false,
								'PHP'=>false,
								'Python'=>false,	
								);

    public function __construct($request) 
    {
        parent::__construct($request);
    }

    public function setLanguage($language) 
    {
        $this->language = $language;
    }
    
    public function getLanguage()
    {
		return $this->language;
	}
	
	public function getType()
    {
		return $this->type;
	}

    protected function afterCompile() {
        
    }

    protected function beforeCompile() {
        $this->targetFilePath = $this->getGradingDirectory() . "/" . $this->requestModel->source_file;
        file_put_contents($this->targetFilePath , $this->requestModel->file);
    }
    
    public function setTarget($target) {
    }
    
    public function addCapabilities($cap)
    {
		$this->capabilities[$cap] = true;
	}

    /**
     * Membuat berkas script (.sh) untuk kemudian dijalankan oleh grader 
     */
    protected function generateExecutionScript() 
    {
        if ($this->compileReturnValue == 0) 
        {
			
            $this->executionScriptPath = $this->gradingDirectory . "/execute.sh";
            $executionCommand = "";
            if ($this->type == "intepreter")
			{
				switch ($this->language)
				{
					case "Python":
					{
						$executionCommand = "python " . $this->targetFilePath;
						break;
					}
					case "PHP":
					{
						$executionCommand = "php " . $this->targetFilePath;
						break;
					}
					case "LISP":
					{
						$executionCommand = "clisp -i " . $this->targetFilePath;
						break;
					}
				}
			}
			else if ($this->type == "compiler")
			{
				if ($this->language == "Java") 
				{
					$targetClassFile = basename($this->outputFilePath, ".class") . "\n";
					$executionCommand = "cd $this->gradingDirectory\n/usr/bin/java $targetClassFile";
				} else 
				{
					$executionCommand = "bin/mo-eval/mo-evalbox -e -w 30 -m 32000000 2>&1 -- " . $this->outputFilePath;
				};
			}
            file_put_contents($this->executionScriptPath, $executionCommand);
            chmod($this->executionScriptPath,0777);
        }
        else 
        {
            $this->executionScriptPath = "";
        }
    }

    protected function doCompile() 
    {
        $ext = end(explode(".", $this->targetFilePath));
        $this->extension = $ext;
        if ($ext != "java")
            $this->outputFilePath = tempnam($this->gradingDirectory, "lz-single-");
        $compileCommand = "";
        switch ($ext) 
        {
            case "cpp":
            {
                $compileCommand = "/usr/bin/g++ -o $this->outputFilePath $this->targetFilePath -lm -v";
                $this->language = "C++";
                $this->type = "compiler";
                break;
			}
            case "cc":
            {
                $compileCommand = "/usr/bin/g++ -o $this->outputFilePath $this->targetFilePath -lm -v";
                $this->language = "C++";
                $this->type = "compiler";
                break;
			}
            case "pas":
            {
                $compileCommand = "/usr/bin/fpc -o $this->outputFilePath $this->targetFilePath";
                $this->language = "Pascal";
                $this->type = "compiler";
                break;
			}
            case "c":
            {
                $compileCommand = "/usr/bin/gcc -o $this->outputFilePath $this->targetFilePath -lm -v";
                $this->language = "C";
                $this->type = "compiler";
				break;
			}
            case "java":
            {
                $compileCommand = "/usr/bin/javac $this->targetFilePath -d " . $this->gradingDirectory;
                $this->outputFilePath = $this->gradingDirectory . "/" . basename($this->targetFilePath, ".java") . ".class";
                $this->language = "Java";
                $this->type = "compiler";
                break;
			}
            case "py":
            {
				$this->language = "Python";
				$this->type = "intepreter";
				break;
			}
			case "php":
			{
				$this->language = "PHP";
				$this->type = "intepreter";
				break;
			}
			case "l":
			{
				$this->language = "LISP";
				$this->type = "intepreter";
				break;
			}
			case "lisp":
			{
				$this->language = "LISP";
				$this->type = "intepreter";
				break;
			}
			case "lsp":
			{
				$this->language = "LISP";
				$this->type = "intepreter";
				break;
			}
        }
        if (($this->type == "compiler") && ($this->capabilities[$this->language]))
        {
			exec("$compileCommand 2>&1", $output, $return);
			$tes = chmod($this->outputFilePath,0777);
			$this->compileOutput = implode("\n", $output);
			$this->compileReturnValue = $return;
		}
		else if (($this->type == "intepreter") && ($this->capabilities[$this->language]))
		{
			$this->compileReturnValue = 0;
		}
		else
		{
			$this->compileReturnValue = -1;
		}
    }

}

?>
