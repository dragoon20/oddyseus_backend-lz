<?php

class ContestantReleaseWidget extends CWidget {

    public $handler;

    public function run() {
        $dataProvider = new CActiveDataProvider('Submission', array(
                    'criteria' => array(
                        'condition' => 'submitter_id = :submitter_id AND contest_id = :contest_id',
                        'params' => array(
                            'submitter_id' => Yii::app()->user->getId(),
                            'contest_id' => $this->handler->getContest()->id
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => 20,
                    ),
                ));

        $this->render('contestantrelease', array('dataProvider' => $dataProvider));
    }

}
