<?php

Yii::import("application.components.evaluators.GraderBase");

/**
 * Kelas konret untuk grader simple
 * Objek SimpleGrader akan menjalankan program hasil kompilasi dan mengujinya dengan test cases
 * Setelah grading, hasil grading dapat diambil berupa array
 */
class SimpleGrader extends GraderBase {

    public $useProvidedInput = false;

    public function __construct($pCompiler) {
        parent::__construct($pCompiler);
    }

    protected function afterGrade() {
        parent::afterGrade();
    }

    protected function beforeGrade() {
        
    }

    protected function doGrade() 
    {
        $gradeOutput = $this->compiler->getGradingDirectory() . "/grade.out";
        
        if ($this->useProvidedInput) 
        {
            file_put_contents($this->compiler->getGradingDirectory() . "/input" , $this->requestModel->input);
            $return = $this->execute("bin/mo-eval/mo-evalbox -e -w 30 -o $gradeOutput -- " . $this->compiler->getOutputFilePath() . " < " . $this->compiler->getGradingDirectory() . "/input" . " 2>&1", $output);
        } else {
            $return = $this->execute("bin/mo-eval/mo-evalbox -e -w 30 -o $gradeOutput -- /bin/sh " . $this->compiler->getExecutionScriptPath() . " 2>&1", $output);
        }
        $this->gradeResult['output'] = file_get_contents($gradeOutput);
        $this->gradeResult['return'] = $return;
        $this->gradeResult['sandboxoutput'] = $output;
    }

}

?>
